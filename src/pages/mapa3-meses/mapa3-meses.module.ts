import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Mapa3MesesPage } from './mapa3-meses';

@NgModule({
  declarations: [
    Mapa3MesesPage,
  ],
  imports: [
    IonicPageModule.forChild(Mapa3MesesPage),
  ],
})
export class Mapa3MesesPageModule {}
