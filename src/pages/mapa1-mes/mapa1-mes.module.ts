import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Mapa1MesPage } from './mapa1-mes';

@NgModule({
  declarations: [
    Mapa1MesPage,
  ],
  imports: [
    IonicPageModule.forChild(Mapa1MesPage),
  ],
})
export class Mapa1MesPageModule {}
