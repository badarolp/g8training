import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Teste2Page } from './teste2';
import { NgCalendarModule } from 'ionic2-calendar';

@NgModule({
  declarations: [
    Teste2Page
  ],
  imports: [
    IonicPageModule.forChild(Teste2Page),
    NgCalendarModule
  ],
  exports: [
    Teste2Page
  ],  
})
export class Teste2PageModule {}
