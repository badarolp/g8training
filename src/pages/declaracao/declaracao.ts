import { UsersProvider } from '../../providers/users/users';
import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController} from 'ionic-angular';
import { Subscription } from 'rxjs/Rx';

@IonicPage()
@Component({
  selector: 'page-declaracao',
  templateUrl: 'declaracao.html',
})
export class DeclaracaoPage {
  declaracao;
  declaracaoSubscription: Subscription;

  constructor(public navCtrl: NavController,
    private usersProvider: UsersProvider,
    private toastCtrl: ToastController) {
  }

  editarDeclaracao() {
    this.usersProvider.updateDeclaracao(this.declaracao).then(() => {
      let toast = this.toastCtrl.create({
        duration: 3000,
        message: 'Declaração editada!'
      });
      toast.present();
    })
  }
  
  ionViewDidLoad() {
    this.declaracaoSubscription = this.usersProvider.getCurrentDeclaracao().subscribe(declaracao => {
      this.declaracao = declaracao;
    });
  }

  navigateToBack(): void {
    this.navCtrl.pop();
  }

  podeSalvar(): boolean {
    if (this.declaracao = "") {
      return false;
    } else
      return true;
  }

}
