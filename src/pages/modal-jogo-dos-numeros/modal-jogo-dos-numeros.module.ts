import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalJogoDosNumerosPage } from './modal-jogo-dos-numeros';

@NgModule({
  declarations: [
    ModalJogoDosNumerosPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalJogoDosNumerosPage),
  ],
  exports: [
    ModalJogoDosNumerosPage
  ]
})
export class ModalJogoDosNumerosPageModule {}
