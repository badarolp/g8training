import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PotencialComunicativoPage } from './potencial-comunicativo';

@NgModule({
  declarations: [
    PotencialComunicativoPage,
  ],
  imports: [
    IonicPageModule.forChild(PotencialComunicativoPage),
  ],
})
export class PotencialComunicativoPageModule {}
