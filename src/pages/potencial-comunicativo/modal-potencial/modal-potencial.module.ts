import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalPotencialPage } from './modal-potencial';

@NgModule({
  declarations: [
    ModalPotencialPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalPotencialPage),
  ],
  exports: [
    ModalPotencialPage
  ]
})
export class PopupFabModalPageModule {}