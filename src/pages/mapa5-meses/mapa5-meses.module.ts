import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Mapa5MesesPage } from './mapa5-meses';

@NgModule({
  declarations: [
    Mapa5MesesPage,
  ],
  imports: [
    IonicPageModule.forChild(Mapa5MesesPage),
  ],
})
export class Mapa5MesesPageModule {}
