//import { Component } from '@angular/core';
//import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
//import * as moment from 'moment';
import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
/**
 * Generated class for the TestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-test',
  templateUrl: 'test.html',
})
export class TestPage {
  timeInSeconds: number = 60;
  seconds: number;
  secondsRemaining: number;
  runTimer: boolean;
  started: boolean;
  finished: boolean;
  displayTime: string;
  
  pontos: number = 0;

  constructor(private alertCtrl: AlertController) {
  }

  ngOnInit() {
    this.initTimer();
  }

  hasFinished() {
    return this.finished;
  }

  initTimer() {
    if (!this.timeInSeconds) { this.timeInSeconds = 0; }

    this.seconds = this.timeInSeconds;
    this.runTimer = false;
    this.started = false;
    this.finished = false;
    this.secondsRemaining = this.timeInSeconds;


    this.displayTime = this.getSecondsAsDigitalClock(this.secondsRemaining);
  }

  startTimer() {
    this.started = true;
    this.runTimer = true;
    this.timerTick();
  }

  pauseTimer() {
    this.runTimer = false;
  }

  resumeTimer() {
    this.startTimer();
  }

  timerTick() {
    setTimeout(() => {
      if (!this.runTimer) { return; }
      this.secondsRemaining--;
      this.displayTime = this.getSecondsAsDigitalClock(this.secondsRemaining);
      if (this.secondsRemaining > 0) {
        this.timerTick();
      } else {
        this.finished = true;
      }
    }, 1000);
  }

  getSecondsAsDigitalClock(inputSeconds: number) {
    const secNum = parseInt(inputSeconds.toString(), 10); // don't forget the second param
    const hours = Math.floor(secNum / 3600);
    const minutes = Math.floor((secNum - (hours * 3600)) / 60);
    const seconds = secNum - (hours * 3600) - (minutes * 60);
    let minutesString = '';
    let secondsString = '';
    minutesString = (minutes < 10) ? '0' + minutes : minutes.toString();
    secondsString = (seconds < 10) ? '0' + seconds : seconds.toString();
    return minutesString + ':' + secondsString;
  }

  botaoCerto() {
    this.pontos = this.pontos + 3;
  }
  
  botaoErrado() {
    this.pontos = this.pontos - 20 + this.secondsRemaining;
    let alert = this.alertCtrl.create({
      title: 'Botão Errado!',
      subTitle: 'Sua pontuação é ' + this.pontos,
      buttons: ['OK']
    });
    alert.present();
    this.pontos = 0;
    this.initTimer();
  }
  
  terminar() {
    let alert = this.alertCtrl.create({
      title: 'Acabou!',
      subTitle: 'Acabou...',
      buttons: ['OK']
    });
    alert.present();
    this.pontos = 0;
    
  }

/*
  event = { startTime: new Date().toISOString(), endTime: new Date().toISOString(), allDay: false };
  minDate = new Date().toISOString();

  constructor(public navCtrl: NavController, private navParams: NavParams, public viewCtrl: ViewController) {
    let preselectedDate = moment(this.navParams.get('selectedDay')).format();
    this.event.startTime = preselectedDate;
    this.event.endTime = preselectedDate;
  }

  cancel() {
    this.viewCtrl.dismiss();
  }

  save() {
    this.viewCtrl.dismiss(this.event);
  }
  */
}
