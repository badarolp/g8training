import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TestPage } from './test';
import { NgCalendarModule } from 'ionic2-calendar';

@NgModule({
  declarations: [
    TestPage
  ],
  imports: [
    IonicPageModule.forChild(TestPage),
    NgCalendarModule    
  ],
  exports: [
    TestPage
  ]
})
export class TestPageModule {}
