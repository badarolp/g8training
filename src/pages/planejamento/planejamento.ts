import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-planejamento',
  templateUrl: 'planejamento.html',
})
export class PlanejamentoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  navigateToPage(pageName): void {
    this.navCtrl.push(pageName);
  }

  navigateToBack(): void {
    this.navCtrl.pop();
  }

}
