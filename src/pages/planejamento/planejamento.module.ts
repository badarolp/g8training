import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlanejamentoPage } from './planejamento';

@NgModule({
  declarations: [
    PlanejamentoPage,
  ],
  imports: [
    IonicPageModule.forChild(PlanejamentoPage),
  ],
})
export class PlanejamentoPageModule {}
