import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComunicacaoPessoalPage } from './comunicacao-pessoal';

@NgModule({
  declarations: [
    ComunicacaoPessoalPage,
  ],
  imports: [
    IonicPageModule.forChild(ComunicacaoPessoalPage),
  ],
})
export class ComunicacaoPessoalPageModule {}
