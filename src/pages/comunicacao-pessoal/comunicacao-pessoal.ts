import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-comunicacao-pessoal',
  templateUrl: 'comunicacao-pessoal.html',
})
export class ComunicacaoPessoalPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  
  navigateToPage(pageName): void {
    this.navCtrl.push(pageName);
  }  

}
