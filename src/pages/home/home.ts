import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';

import { Subscription } from 'rxjs/Rx';
import { UsersProvider } from '../../providers/users/users';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  nickname;
  nicknameSubscription: Subscription;

  constructor(private afAuth: AngularFireAuth,
    private toast: ToastController,
    public navCtrl: NavController,
    private usersProvider: UsersProvider) {

  }

  navigateToPage(pageName: string): void {  
    this.navCtrl.push(pageName);
  }

  ionViewWillLoad() {

    this.nicknameSubscription = this.usersProvider.getCurrentUserNickname().subscribe(nickname => {
      this.nickname = nickname;
    });

    this.afAuth.authState.subscribe(data => {
      if (data && data.email && data.uid) {
        this.toast.create({
          message: `Bem vindo ao G8 Training, ${data.email}`,
          duration: 3000
        }).present();
      }
      else {
        this.toast.create({
          message: `Não foi possível encontrar detalhes de autenticação`,
          duration: 3000
        }).present();
      }
    } 
    );
  }

  ionViewWillInload() {
    this.nicknameSubscription.unsubscribe();
  }  

  metodo() {
    this.usersProvider.metodo();
  }

}
