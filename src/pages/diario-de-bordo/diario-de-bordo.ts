import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-diario-de-bordo',
  templateUrl: 'diario-de-bordo.html',
})
export class DiarioDeBordoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  navigateToBack(): void {
    this.navCtrl.pop();
  }  

}
