import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Mapa4MesesPage } from './mapa4-meses';

@NgModule({
  declarations: [
    Mapa4MesesPage,
  ],
  imports: [
    IonicPageModule.forChild(Mapa4MesesPage),
  ],
})
export class Mapa4MesesPageModule {}
