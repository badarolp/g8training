import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Mapa2MesesPage } from './mapa2-meses';

@NgModule({
  declarations: [
    Mapa2MesesPage,
  ],
  imports: [
    IonicPageModule.forChild(Mapa2MesesPage),
  ],
})
export class Mapa2MesesPageModule {}
