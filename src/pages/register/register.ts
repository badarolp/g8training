import { UserCredentials, UsersProvider } from '../../providers/users/users';
import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, ToastController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  creds = new UserCredentials();

  constructor(public navCtrl: NavController,
    private usersProvider: UsersProvider,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController) { }

  register() {
    this.usersProvider.signUp(this.creds).then(res => {
      let toast = this.toastCtrl.create({
        duration: 3000,
        message: 'Conta criada com sucesso!'
      });
      toast.present();
    }, err => {
      let alert = this.alertCtrl.create({
        title: 'Erro',
        message: this.getErr(err.message),
        buttons: ['OK']
      });
      alert.present();
    }) 
  }

  getErr(err: string): string{
    return "teste";
  }
  navigateToBack(): void {
    this.navCtrl.pop();
  }

}
