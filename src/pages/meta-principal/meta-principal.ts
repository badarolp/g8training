import { UsersProvider } from '../../providers/users/users';
import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { Subscription } from 'rxjs/Rx';


@IonicPage()
@Component({
  selector: 'page-meta-principal',
  templateUrl: 'meta-principal.html',
})
export class MetaPrincipalPage {
  meta;
  metaSubscription: Subscription;

  constructor(public navCtrl: NavController,
    private usersProvider: UsersProvider,
    private toastCtrl: ToastController) {
  }

  editarMeta() {
    this.usersProvider.updateMeta(this.meta).then(() => {
      let toast = this.toastCtrl.create({
        duration: 3000,
        message: 'Meta editada!'
      });
      toast.present();
    })
  }

  ionViewDidLoad() {
    this.metaSubscription = this.usersProvider.getCurrentMeta().subscribe(meta => {
      this.meta = meta;
    });
  }

  navigateToBack(): void {
    this.navCtrl.pop();
  }

}
