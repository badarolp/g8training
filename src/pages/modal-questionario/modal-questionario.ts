import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Slides, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'modal-questionario',
  templateUrl: 'modal-questionario.html',
})
export class ModalQuestionarioPage {

  @ViewChild('slider') slider: Slides;
  slideIndex = 0;
  slides = [
    {
      title: 'Vamos lá!',
      description: 'Esse questionário ajudará a descobrir seu perfil comportamental',
      img: "assets/imgs/puzzle.png",   
    },
    {
      description: 'Escolha 9 atitudes que você considera fazerem parte do seu perfil',
      img: "assets/imgs/cerebro.png", 
    }
  ];

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController) { }

  onSlideChanged() {
    this.slideIndex = this.slider.getActiveIndex();
  }

  goToApp() {
    this.dismiss();
  }

  skip() {
    this.dismiss();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }  
}
