import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import chartJs from 'chart.js';

@IonicPage()
@Component({
  selector: 'page-roda-da-vida',
  templateUrl: 'roda-da-vida.html',
})
export class RodaDaVidaPage {

  @ViewChild('radarCanvas') radarCanvas: ElementRef;
  @ViewChild('polarCanvas') polarCanvas: ElementRef;

  radarChart: any;
  polarAreaChart: any;

  constructor(public navCtrl: NavController) { }

  ngAfterViewInit() {
    setTimeout(() => {
      this.radarChart = this.getRadarChart();
      this.polarAreaChart = this.getPolarAreaChart();
    }, 250);
  }

  getChart(context, chartType, data, options?) {
    return new chartJs(context, {
      data,
      options,
      type: chartType,
    });
  }

  getPolarAreaChart() {
    const data = {
      datasets: [{
        data: [2, 5, 7, 3, 8, 5],
        backgroundColor: ['#FF6384', '#4BC0C0', '#FFCE56', '#E7E9ED', '#36A2EB'],
        label: 'My dataset' // for the legend
      }],
      labels: ['Dominância', 'Automotivação', 'Sociabilidade', 'Des.relações', 'Exctroversão']
    };

    const options = {
      elements: {
        arc: {
          borderColor: '#000000'
        }
      }
    };

    return this.getChart(this.polarCanvas.nativeElement, 'polarArea', data, options);
  }

  getRadarChart() {
    const data = {
      labels: ['Dominância', 'Automotivação', 'Sociabilidade', 'Des.relações', 'Exctroversão'],
      datasets: [
        {
          label: 'Lucas',
          backgroundColor: 'rgba(255,99,132,0.2)',
          borderColor: 'rgba(255,99,132,1)',
          pointBackgroundColor: 'rgba(255,99,132,1)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(255,99,132,1)',
          data: [2, 5, 7, 3, 8, 5]
        }
      ]
    };

    const options = {
      scale: {
        reverse: true,
        ticks: {
          beginAtZero: true
        }
      }
    };

    return this.getChart(this.radarCanvas.nativeElement, 'radar', data, options);
  }

  navigateToPage(pageName): void {
    this.navCtrl.push(pageName);
  }

  navigateToBack(): void {
    this.navCtrl.pop();
  }  

}