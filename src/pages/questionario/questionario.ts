import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-questionario',
  templateUrl: 'questionario.html',
})
export class QuestionarioPage {
  atitudes: Array<{ descricao: string, perfil: number, marcada: boolean}>;
  count: number = 9;
  botao: string;
  countValores: Array<{ perfil: number, count: number, descricao: string }>;
  persona: string = '';

  expanded: any;
  contracted: any;
  showIcon = true;
  preload = true;    

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController) {

  }

  buildAtitudes() {
    this.atitudes = [
      { descricao: 'AUTO CONFIANTE', perfil: 0, marcada: false },
      { descricao: 'ENÉRGICO E DOMINANTE', perfil: 0, marcada: false },
      { descricao: 'ACEITA E GOSTA DE DESAFIOS', perfil: 0, marcada: false },
      { descricao: 'COMPETITIVO E AUDACIOSO', perfil: 0, marcada: false },
      { descricao: 'DESTEMIDO E CORAJOSO', perfil: 0, marcada: false },
      //
      { descricao: 'COMUNICATIVO', perfil: 1, marcada: false },
      { descricao: 'PERSUASIVO', perfil: 1, marcada: false },
      { descricao: 'ENTUSIASMADO', perfil: 1, marcada: false },
      { descricao: 'OTIMISTA', perfil: 1, marcada: false },
      { descricao: 'FÁCIL DE RELACIONAR', perfil: 1, marcada: false },
      //
      { descricao: 'ESTÁVEL', perfil: 2, marcada: false },
      { descricao: 'PACIENTE', perfil: 2, marcada: false },
      { descricao: 'CALMO', perfil: 2, marcada: false },
      { descricao: 'TEM RITMO CONSTANTE', perfil: 2, marcada: false },
      { descricao: 'CONSERVADOR', perfil: 2, marcada: false },
      //
      { descricao: 'PRECISO', perfil: 3, marcada: false },
      { descricao: 'ATENTO AOS DETALHES', perfil: 3, marcada: false },
      { descricao: 'DILIGENTE', perfil: 3, marcada: false },
      { descricao: 'ORGANIZADO', perfil: 3, marcada: false },
      { descricao: 'RESPONSÁVEL', perfil: 3, marcada: false }
    ];
  }

  buildValores() {
    this.countValores = [
      { perfil: 0, count: 0, descricao: 'Executor' },
      { perfil: 1, count: 0, descricao: 'Comunicador' },
      { perfil: 2, count: 0, descricao: 'Planejador' },
      { perfil: 3, count: 0, descricao: 'Analista' }
    ];
  }

  updateCount(atitude) {
    atitude.marcada = !atitude.marcada;
    if (atitude.marcada) {
      this.count--;
    } else {
      this.count++;
    }
    this.updateBotao();
  }

  isDisable(checkbox: boolean): boolean {
    if (checkbox) {
      return false;
    } else {
      if (this.count == 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  updateBotao() {
    if (this.count == 0) {
      this.botao = "Salvar";
    } else if (this.count == 1) {
      this.botao = "Falta 1...";
    } else {
      this.botao = "Faltam " + this.count + "...";
    }
  }

  canSalvar() {
    return this.count == 0;
  }

  salvar() {
    this.buildValores();
    for (let atitude of this.atitudes) {
      if (atitude.marcada) {
        this.countValores[atitude.perfil].count++;
      }
    }
    
    this.countValores.sort(function (a, b) {
      if (a.count < b.count) {
        return 1;
      }
      if (a.count > b.count) {
        return -1;
      }
      return 0;
    });

    if ((this.countValores[0].count - this.countValores[1].count) >= 2) {
      this.persona = this.countValores[0].descricao;
    } else {
      this.persona = this.countValores[0].descricao + ' e ' + this.countValores[1].descricao;
    }
    
    const alert = this.alertCtrl.create({
      title: 'Você é,',
      subTitle: this.persona + '!',
      buttons: ['Ok']
    });
    alert.present();
  }

  ionViewDidLoad() {
    this.expand();
    this.buildAtitudes();
    this.buildValores();
    this.updateBotao();
  }

  expand() {
    this.expanded = true;
    this.contracted = !this.expanded;
    this.showIcon = false;
    setTimeout(() => {
      const modal = this.modalCtrl.create('ModalQuestionarioPage');
      modal.onDidDismiss(data => {
        this.expanded = false;
        this.contracted = !this.expanded;
        setTimeout(() => this.showIcon = true, 330);
      });
      modal.present();
    }, 200);
  }  

}
