import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Subscription } from 'rxjs/Rx';
import { UsersProvider } from '../../providers/users/users';

@IonicPage()
@Component({
  selector: 'page-dash-meta-principal',
  templateUrl: 'dash-meta-principal.html',
})
export class DashMetaPrincipalPage {
  meta;
  metaSubscription: Subscription;
  nome;
  nomeSubscription: Subscription;

  constructor(public navCtrl: NavController,
    private usersProvider: UsersProvider) {
  }
  
  ionViewWillLoad() {
    this.metaSubscription = this.usersProvider.getCurrentMeta().subscribe(meta => {
      this.meta = meta;
    });
    this.nomeSubscription = this.usersProvider.getCurrentUserNickname().subscribe(nome => {
      this.nome = nome;
    });
  }
  
  navigateToBack(): void {
    this.navCtrl.pop();
  }

}
