import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DashMetaPrincipalPage } from './dash-meta-principal';

@NgModule({
  declarations: [
    DashMetaPrincipalPage,
  ],
  imports: [
    IonicPageModule.forChild(DashMetaPrincipalPage),
  ],
})
export class DashMetaPrincipalPageModule {}
