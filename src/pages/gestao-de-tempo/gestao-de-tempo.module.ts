import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GestaoDeTempoPage } from './gestao-de-tempo';

@NgModule({
  declarations: [
    GestaoDeTempoPage,
  ],
  imports: [
    IonicPageModule.forChild(GestaoDeTempoPage),
  ],
})
export class GestaoDeTempoPageModule {}
