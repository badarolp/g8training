import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JogoDosNumerosPage } from './jogo-dos-numeros';

@NgModule({
  declarations: [
    JogoDosNumerosPage
  ],
  imports: [
    IonicPageModule.forChild(JogoDosNumerosPage)
  ],
})
export class JogoDosNumerosPageModule { }

