import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-jogo-dos-numeros',
  templateUrl: 'jogo-dos-numeros.html',
})
export class JogoDosNumerosPage {
  timeInSeconds: number = 60;
  seconds: number;
  secondsRemaining: number;
  runTimer: boolean;
  started: boolean;
  finished: boolean;
  displayTime: string;
  pontos: number = 0;

  tempoEmSegundos: number = 60;

  expanded: any;
  contracted: any;
  showIcon = true;
  preload = true;  
  
  pressedColor: string = '#ADFF2F';
  wrongColor: string = '#FF5151';
  defaultColor: string = '#D1D4F6';

  degreeStyle10: string = 'rotate(10deg)';
  degreeStyle20: string = 'rotate(20deg)';
  degreeStyle30: string = 'rotate(30deg)';
  degreeStyle40: string = 'rotate(40deg)';
  degreeStyle50: string;
  degreeStyle60: string;
  degreeStyle70: string;
  degreeStyle80: string;
  degreeStyle90: string;
  degreeStyle100: string;
  degreeStyle110: string;
  degreeStyle120: string;
  degreeStyle130: string;
  degreeStyle140: string;
  degreeStyle150: string;
  degreeStyle160: string;
  degreeStyle170: string;
  degreeStyle180: string;
  degreeStyle190: string;
  degreeStyle200: string;

  buttonColor1: string = '#D1D4F6';
  buttonColor2: string = '#D1D4F6';
  buttonColor3: string = '#D1D4F6';
  buttonColor4: string = '#D1D4F6';
  buttonColor5: string = '#D1D4F6';
  buttonColor6: string = '#D1D4F6';
  buttonColor7: string = '#D1D4F6';
  buttonColor8: string = '#D1D4F6';
  buttonColor9: string = '#D1D4F6';
  buttonColor10: string = '#D1D4F6';
  buttonColor11: string = '#D1D4F6';
  buttonColor12: string = '#D1D4F6';
  buttonColor13: string = '#D1D4F6';
  buttonColor14: string = '#D1D4F6';
  buttonColor15: string = '#D1D4F6';
  buttonColor16: string = '#D1D4F6';
  buttonColor17: string = '#D1D4F6';
  buttonColor18: string = '#D1D4F6';
  buttonColor19: string = '#D1D4F6';
  buttonColor20: string = '#D1D4F6';
  buttonColor21: string = '#D1D4F6';
  buttonColor22: string = '#D1D4F6';
  buttonColor23: string = '#D1D4F6';
  buttonColor24: string = '#D1D4F6';
  buttonColor25: string = '#D1D4F6';
  buttonColor26: string = '#D1D4F6';
  buttonColor27: string = '#D1D4F6';
  buttonColor28: string = '#D1D4F6';
  buttonColor29: string = '#D1D4F6';
  buttonColor30: string = '#D1D4F6';
  buttonColor31: string = '#D1D4F6';
  buttonColor32: string = '#D1D4F6';
  buttonColor33: string = '#D1D4F6';
  buttonColor34: string = '#D1D4F6';
  buttonColor35: string = '#D1D4F6';
  buttonColor36: string = '#D1D4F6';
  buttonColor37: string = '#D1D4F6';
  buttonColor38: string = '#D1D4F6';
  buttonColor39: string = '#D1D4F6';
  buttonColor40: string = '#D1D4F6';
  buttonColor41: string = '#D1D4F6';
  buttonColor42: string = '#D1D4F6';
  buttonColor43: string = '#D1D4F6';
  buttonColor44: string = '#D1D4F6';
  buttonColor45: string = '#D1D4F6';
  buttonColor46: string = '#D1D4F6';
  buttonColor47: string = '#D1D4F6';
  buttonColor48: string = '#D1D4F6';
  buttonColor49: string = '#D1D4F6';
  buttonColor50: string = '#D1D4F6';

  //buttoAtual: string;
  
  numeroAtual: number = 1;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private modalCtrl: ModalController,
    private alertCtrl: AlertController) {
  }

  navigateToPage(pageName): void {
    this.navCtrl.push(pageName);
  }

  navigateToBack(): void {
    this.navCtrl.pop();
  }

  paintButton(button: string) {
    var vColor: string;
    var vOk: boolean = this.numeroAtual == Number(button);

    if (vOk) {
      vColor = this.pressedColor;
      if (this.numeroAtual == 50) {
        this.terminar();
      } else {
        this.numeroAtual++;
        this.botaoCerto();
      }
    }
    else {
      vColor = this.wrongColor;
      this.botaoErrado();
    }

    switch (button) {
      case ("1"): {
        this.buttonColor1 = vColor;
        break;
      }
      case ("2"): {
        this.buttonColor2 = vColor;
        break;
      }
      case ("3"): {
        this.buttonColor3 = vColor;
        break;
      }
      case ("4"): {
        this.buttonColor4 = vColor;
        break;
      }
      case ("5"): {
        this.buttonColor5 = vColor;
        break;
      }
      case ("6"): {
        this.buttonColor6 = vColor;
        break;
      }
      case ("7"): {
        this.buttonColor7 = vColor;
        break;
      }
      case ("8"): {
        this.buttonColor8 = vColor;
        break;
      }
      case ("9"): {
        this.buttonColor9 = vColor;
        break;
      }
      case ("10"): {
        this.buttonColor10 = vColor;
        break;
      }
      case ("11"): {
        this.buttonColor11 = vColor;
        break;
      }
      case ("12"): {
        this.buttonColor12 = vColor;
        break;
      }
      case ("13"): {
        this.buttonColor13 = vColor;
        break;
      }
      case ("14"): {
        this.buttonColor14 = vColor;
        break;
      }
      case ("15"): {
        this.buttonColor15 = vColor;
        break;
      }
      case ("16"): {
        this.buttonColor16 = vColor;
        break;
      }
      case ("17"): {
        this.buttonColor17 = vColor;
        break;
      }
      case ("18"): {
        this.buttonColor18 = vColor;
        break;
      }
      case ("19"): {
        this.buttonColor19 = vColor;
        break;
      }
      case ("20"): {
        this.buttonColor20 = vColor;
        break;
      }
      case ("21"): {
        this.buttonColor21 = vColor;
        break;
      }
      case ("22"): {
        this.buttonColor22 = vColor;
        break;
      }
      case ("23"): {
        this.buttonColor23 = vColor;
        break;
      }
      case ("24"): {
        this.buttonColor24 = vColor;
        break;
      }
      case ("25"): {
        this.buttonColor25 = vColor;
        break;
      }
      case ("26"): {
        this.buttonColor26 = vColor;
        break;
      }
      case ("27"): {
        this.buttonColor27 = vColor;
        break;
      }
      case ("28"): {
        this.buttonColor28 = vColor;
        break;
      }
      case ("29"): {
        this.buttonColor29 = vColor;
        break;
      }
      case ("30"): {
        this.buttonColor30 = vColor;
        break;
      }
      case ("31"): {
        this.buttonColor31 = vColor;
        break;
      }
      case ("32"): {
        this.buttonColor32 = vColor;
        break;
      }
      case ("33"): {
        this.buttonColor33 = vColor;
        break;
      }
      case ("34"): {
        this.buttonColor34 = vColor;
        break;
      }
      case ("35"): {
        this.buttonColor35 = vColor;
        break;
      }
      case ("36"): {
        this.buttonColor36 = vColor;
        break;
      }
      case ("37"): {
        this.buttonColor37 = vColor;
        break;
      }
      case ("38"): {
        this.buttonColor38 = vColor;
        break;
      }
      case ("39"): {
        this.buttonColor39 = vColor;
        break;
      }
      case ("40"): {
        this.buttonColor40 = vColor;
        break;
      }
      case ("41"): {
        this.buttonColor41 = vColor;
        break;
      }
      case ("42"): {
        this.buttonColor42 = vColor;
        break;
      }
      case ("43"): {
        this.buttonColor43 = vColor;
        break;
      }
      case ("44"): {
        this.buttonColor44 = vColor;
        break;
      }
      case ("45"): {
        this.buttonColor45 = vColor;
        break;
      }
      case ("46"): {
        this.buttonColor46 = vColor;
        break;
      }
      case ("47"): {
        this.buttonColor47 = vColor;
        break;
      }
      case ("48"): {
        this.buttonColor48 = vColor;
        break;
      }
      case ("49"): {
        this.buttonColor49 = vColor;
        break;
      }
      case ("50"): {
        this.buttonColor50 = vColor;
        break;
      }
    }
    
  }
    
  press(button: string)  {
    if (this.started) {
      this.paintButton(button);
    }
  }

  expand() {
    this.expanded = true;
    this.contracted = !this.expanded;
    this.showIcon = false;
    setTimeout(() => {
      const modal = this.modalCtrl.create('ModalJogoDosNumerosPage');
      modal.onDidDismiss(data => {
        this.expanded = false;
        this.contracted = !this.expanded;
        setTimeout(() => this.showIcon = true, 330);
      });
      modal.present();
    }, 200);
  }

  ngOnInit() {
    this.initTimer();
  }

  hasFinished() {
    return this.finished;
  }

  initTimer() {
    if (!this.timeInSeconds) { this.timeInSeconds = 0; }
    this.seconds = this.timeInSeconds;
    this.runTimer = false;
    this.started = false;
    this.finished = false;
    this.secondsRemaining = this.timeInSeconds;
    this.displayTime = this.getSecondsAsDigitalClock(this.secondsRemaining);
  }

  startTimer() {
        
    this.initGame();
    this.started = true;
    this.runTimer = true;
    this.timerTick();
  }

  pauseTimer() {
    this.runTimer = false;
  }

  resumeTimer() {
    this.startTimer();
  }

  timerTick() {
    setTimeout(() => {
      if (!this.runTimer) { return; }
      this.secondsRemaining--;
      this.displayTime = this.getSecondsAsDigitalClock(this.secondsRemaining);
      if (this.secondsRemaining > 0) {
        this.timerTick();
      } else {
        this.finished = true;
        this.terminar();
      }
    }, 1000);
  }

  getSecondsAsDigitalClock(inputSeconds: number) {
    const secNum = parseInt(inputSeconds.toString(), 10); // don't forget the second param
    const hours = Math.floor(secNum / 3600);
    const minutes = Math.floor((secNum - (hours * 3600)) / 60);
    const seconds = secNum - (hours * 3600) - (minutes * 60);
    let minutesString = '';
    let secondsString = '';
    minutesString = (minutes < 10) ? '0' + minutes : minutes.toString();
    secondsString = (seconds < 10) ? '0' + seconds : seconds.toString();
    return minutesString + ':' + secondsString;
  }

  botaoCerto() {
    this.pontos = this.pontos + 3;
  }

  botaoErrado() {
    this.pontos = this.pontos - 20 + this.secondsRemaining;
    let alert = this.alertCtrl.create({
      title: 'Botão errado!',
      subTitle: 'Sua pontuação é ' + this.pontos,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.initGame();
          }
        }
      ]
    });
    alert.present();
    this.pontos = 0;
    this.initTimer();
  }

  terminar() {
    let alert = this.alertCtrl.create({
      title: 'Acabou o tempo!',
      subTitle: 'Sua pontuação é ' + this.pontos,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.initGame();
          }
        }
      ]
    });
    alert.present();
    this.pontos = 0;
    this.initTimer();
  }

  initGame() {
    this.numeroAtual = 1;
    this.pontos = 0;
    this.buttonColor1 = '#D1D4F6';
    this.buttonColor2 = '#D1D4F6';
    this.buttonColor3 = '#D1D4F6';
    this.buttonColor4 = '#D1D4F6';
    this.buttonColor5 = '#D1D4F6';
    this.buttonColor6 = '#D1D4F6';
    this.buttonColor7 = '#D1D4F6';
    this.buttonColor8 = '#D1D4F6';
    this.buttonColor9 = '#D1D4F6';
    this.buttonColor10 = '#D1D4F6';
    this.buttonColor11 = '#D1D4F6';
    this.buttonColor12 = '#D1D4F6';
    this.buttonColor13 = '#D1D4F6';
    this.buttonColor14 = '#D1D4F6';
    this.buttonColor15 = '#D1D4F6';
    this.buttonColor16 = '#D1D4F6';
    this.buttonColor17 = '#D1D4F6';
    this.buttonColor18 = '#D1D4F6';
    this.buttonColor19 = '#D1D4F6';
    this.buttonColor20 = '#D1D4F6';
    this.buttonColor21 = '#D1D4F6';
    this.buttonColor22 = '#D1D4F6';
    this.buttonColor23 = '#D1D4F6';
    this.buttonColor24 = '#D1D4F6';
    this.buttonColor25 = '#D1D4F6';
    this.buttonColor26 = '#D1D4F6';
    this.buttonColor27 = '#D1D4F6';
    this.buttonColor28 = '#D1D4F6';
    this.buttonColor29 = '#D1D4F6';
    this.buttonColor30 = '#D1D4F6';
    this.buttonColor31 = '#D1D4F6';
    this.buttonColor32 = '#D1D4F6';
    this.buttonColor33 = '#D1D4F6';
    this.buttonColor34 = '#D1D4F6';
    this.buttonColor35 = '#D1D4F6';
    this.buttonColor36 = '#D1D4F6';
    this.buttonColor37 = '#D1D4F6';
    this.buttonColor38 = '#D1D4F6';
    this.buttonColor39 = '#D1D4F6';
    this.buttonColor40 = '#D1D4F6';
    this.buttonColor41 = '#D1D4F6';
    this.buttonColor42 = '#D1D4F6';
    this.buttonColor43 = '#D1D4F6';
    this.buttonColor44 = '#D1D4F6';
    this.buttonColor45 = '#D1D4F6';
    this.buttonColor46 = '#D1D4F6';
    this.buttonColor47 = '#D1D4F6';
    this.buttonColor48 = '#D1D4F6';
    this.buttonColor49 = '#D1D4F6';
    this.buttonColor50 = '#D1D4F6';     
  }

  ionViewCanLeave() {
    this.initTimer();
  }

  ionViewCanEnter() {
    this.expand();
  }

}