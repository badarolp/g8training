import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable, BehaviorSubject } from 'rxjs';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

export class UserCredentials {
  nickname = '';
  email = '';
  password = '';
}

@Injectable()
export class UsersProvider {

  
  size$: BehaviorSubject<string | null>;

  
  authState = this.afAuth.authState;
  usersRef = this.db.list('users');
  g8Ref = this.db.list("g8");
  modulo1Ref = this.db.list("modulo1");
  m2QuestionarioRef = this.db.list("m2Questionario");
  m3MapaDeTalentosRef = this.db.list("m3MapaDeTalentos");

  itemsRef: AngularFireList<any>;
  items: Observable<any[]>;  
  
  constructor(public http: Http,
    private afAuth: AngularFireAuth,
    private db: AngularFireDatabase) {
  }

  signUp(credentials: UserCredentials) {
    return this.afAuth.auth.createUserWithEmailAndPassword(credentials.email, credentials.password)
      .then(() => {
        return this.usersRef.set(this.afAuth.auth.currentUser.uid, {
          email: credentials.email,
          nickname: credentials.nickname
        }), this.g8Ref.set(this.afAuth.auth.currentUser.uid, {
          criador: credentials.email
        }), this.modulo1Ref.set(this.afAuth.auth.currentUser.uid, {
          declaracao: '',
          meta: '',
          hasDeclaracao: false,
          hasMeta: false
        }), this.m2QuestionarioRef.set(this.afAuth.auth.currentUser.uid, {
          naoSei: ''
        }), this.m3MapaDeTalentosRef.set(this.afAuth.auth.currentUser.uid, {
          naoSei: ''
        })

      })    
  }

  newG8() {
    
    //return this.db.object('g8/' + '').valueChanges();
    //return this.db.list('g8/').set('qqoCCFd8o3YmNaGy6wVQxNiCBPo2', { creator: 'z@z.com', num: '2' });
    //return this.db.list('g8/qqoCCFd8o3YmNaGy6wVQxNiCBPo2').push({ creator: 'z@z.com', num: '2' });
    //return this.db.list('/g8/-L3O8oiTc37hHUkDlOWy/modulo1').push({ declaracao: null, num: null });
    
  }

  newModulo1() {
    return this.db.list('modulo1/').set('-L3O8oiTc37hHUkDlOWy', { declaracao: '', meta: '', id: '-L3O8oiTc37hHUkDlOWy' });
  }

  newStruct() {

  }

  hasObjetivoDeclaracao(): boolean {
    return true;
  }

  signIn(credentials: UserCredentials) {
    return this.afAuth.auth.signInWithEmailAndPassword(credentials.email, credentials.password);
  }

  signOut() {
    this.afAuth.auth.signOut();
  }

  resetPw(email) {
    return this.afAuth.auth.sendPasswordResetEmail(email);
  }

  getCurrentUserMail() {
    return this.getUserMail(this.afAuth.auth.currentUser.uid);
  }

  getUserMail(id) {
    return this.db.object('users/' + id + '/email').valueChanges();
  }

  getCurrentUserNickname() {
    return this.getUserNickname(this.afAuth.auth.currentUser.uid);
  }

  getUserNickname(id) {
    return this.db.object('users/' + id + '/nickname').valueChanges();
  }

  updateNickname(newName) {
    return this.db.object('users/' + this.afAuth.auth.currentUser.uid).update({ nickname: newName });
  }

  getCurrentDeclaracao() {
    return this.getDeclaracao(this.afAuth.auth.currentUser.uid);
  }

  getDeclaracao(id) {
    return this.db.object('modulo1/' + id + '/declaracao').valueChanges();
  }

  updateDeclaracao(newDeclaracao) {
    return this.db.object('modulo1/' + this.afAuth.auth.currentUser.uid).update({ declaracao: newDeclaracao });
  }

  getCurrentMeta() {
    return this.getMeta(this.afAuth.auth.currentUser.uid);
  }

  getMeta(id) {
    return this.db.object('modulo1/' + id + '/meta').valueChanges();
  }  

  updateMeta(newMeta) {
    return this.db.object('modulo1/' + this.afAuth.auth.currentUser.uid).update({ meta: newMeta });
  }

  metodo() {

  }
}
