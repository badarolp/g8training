webpackJsonp([30],{

/***/ 457:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeModule", function() { return HomeModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home__ = __webpack_require__(679);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var HomeModule = (function () {
    function HomeModule() {
    }
    return HomeModule;
}());
HomeModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]
        ]
    })
], HomeModule);

//# sourceMappingURL=home.module.js.map

/***/ }),

/***/ 679:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_users_users__ = __webpack_require__(135);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomePage = (function () {
    function HomePage(afAuth, toast, navCtrl, usersProvider) {
        this.afAuth = afAuth;
        this.toast = toast;
        this.navCtrl = navCtrl;
        this.usersProvider = usersProvider;
    }
    HomePage.prototype.navigateToPage = function (pageName) {
        this.navCtrl.push(pageName);
    };
    HomePage.prototype.ionViewWillLoad = function () {
        var _this = this;
        this.nicknameSubscription = this.usersProvider.getCurrentUserNickname().subscribe(function (nickname) {
            _this.nickname = nickname;
        });
        this.afAuth.authState.subscribe(function (data) {
            if (data && data.email && data.uid) {
                _this.toast.create({
                    message: "Bem vindo ao G8 Training, " + data.email,
                    duration: 3000
                }).present();
            }
            else {
                _this.toast.create({
                    message: "N\u00E3o foi poss\u00EDvel encontrar detalhes de autentica\u00E7\u00E3o",
                    duration: 3000
                }).present();
            }
        });
    };
    HomePage.prototype.ionViewWillInload = function () {
        this.nicknameSubscription.unsubscribe();
    };
    HomePage.prototype.metodo = function () {
        this.usersProvider.metodo();
    };
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-home',template:/*ion-inline-start:"D:\Projects\g8training\src\pages\home\home.html"*/'<ion-header no-border>\n\n  <ion-navbar color="primary">\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title text-center>G8 TRAINING</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="list-page">\n\n\n\n  <div class="logo-home-container">\n\n      <h2>Bem vindo, {{nickname}}!</h2>\n\n      <img class="logo-home" src="assets/imgs/logog8.png">\n\n  </div>\n\n    \n\n  <ion-list>\n\n\n\n    <!--\n\n    <ion-item tappable (click)="metodo()">\n\n      <ion-thumbnail item-start>\n\n        <img src="assets/imgs/logo.png">\n\n      </ion-thumbnail>\n\n      <h2>Metodo</h2>\n\n    </ion-item>\n\n    -->\n\n    <ion-item tappable (click)="navigateToPage(\'ModulosPage\')">\n\n      <ion-thumbnail item-start>\n\n        <img src="assets/imgs/modulos.png">\n\n      </ion-thumbnail>\n\n      <h2>Módulos</h2>\n\n    </ion-item>\n\n\n\n    <ion-item tappable (click)="navigateToPage(\'DashMetaPrincipalPage\')">\n\n      <ion-thumbnail item-start>\n\n          <img src="assets/imgs/meta.png">\n\n      </ion-thumbnail>\n\n      <h2>Meta Principal</h2>\n\n    </ion-item>\n\n\n\n    <!--<ion-item tappable (click)="navigateToPage(\'DiarioDeBordoPage\')">-->\n\n    <ion-item tappable>\n\n      <ion-thumbnail item-start>\n\n        <!--<img src="assets/imgs/diario.png">-->\n\n        <img src="assets/imgs/logo.png">\n\n      </ion-thumbnail>\n\n      <h2>Diário de Bordo</h2>\n\n    </ion-item>\n\n    \n\n    <ion-item tappable (click)="navigateToPage(\'MensagensPage\')">\n\n      <ion-thumbnail item-start>\n\n        <img src="assets/imgs/chat.png">\n\n      </ion-thumbnail>\n\n      <h2>Mensagens</h2>\n\n    </ion-item>\n\n    \n\n    <ion-item tappable>\n\n      <ion-thumbnail item-start>\n\n        <img src="assets/imgs/logo.png">\n\n      </ion-thumbnail>\n\n      <h2>Ações</h2>\n\n    </ion-item>\n\n\n\n    <ion-item tappable (click)="navigateToPage(\'CalendarPage\')">\n\n      <ion-thumbnail item-start>\n\n        <img src="assets/imgs/calendario.png">\n\n      </ion-thumbnail>\n\n      <h2>Calendário</h2>\n\n    </ion-item>\n\n\n\n    <!--\n\n\n\n    <ion-item tappable (click)="navigateToPage(\'SlideWalkthroughPage\')">\n\n      <ion-thumbnail item-start>\n\n        <img src="assets/imgs/logo.png">\n\n      </ion-thumbnail>\n\n      <h2>Test</h2>\n\n    </ion-item>\n\n\n\n    -->\n\n\n\n    <ion-item tappable (click)="navigateToPage(\'TestPage\')">\n\n      <ion-thumbnail item-start>\n\n        <img src="assets/imgs/logo.png">\n\n      </ion-thumbnail>\n\n      <h2>Test2</h2>\n\n    </ion-item>\n\n    \n\n    \n\n\n\n  </ion-list>\n\n\n\n</ion-content>'/*ion-inline-end:"D:\Projects\g8training\src\pages\home\home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["a" /* AngularFireAuth */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3__providers_users_users__["b" /* UsersProvider */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ })

});
//# sourceMappingURL=30.js.map