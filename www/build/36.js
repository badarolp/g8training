webpackJsonp([36],{

/***/ 451:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComunicacaoPessoalPageModule", function() { return ComunicacaoPessoalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__comunicacao_pessoal__ = __webpack_require__(673);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ComunicacaoPessoalPageModule = (function () {
    function ComunicacaoPessoalPageModule() {
    }
    return ComunicacaoPessoalPageModule;
}());
ComunicacaoPessoalPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__comunicacao_pessoal__["a" /* ComunicacaoPessoalPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__comunicacao_pessoal__["a" /* ComunicacaoPessoalPage */]),
        ],
    })
], ComunicacaoPessoalPageModule);

//# sourceMappingURL=comunicacao-pessoal.module.js.map

/***/ }),

/***/ 673:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComunicacaoPessoalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ComunicacaoPessoalPage = (function () {
    function ComunicacaoPessoalPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ComunicacaoPessoalPage.prototype.navigateToPage = function (pageName) {
        this.navCtrl.push(pageName);
    };
    return ComunicacaoPessoalPage;
}());
ComunicacaoPessoalPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-comunicacao-pessoal',template:/*ion-inline-start:"D:\Projects\g8training\src\pages\comunicacao-pessoal\comunicacao-pessoal.html"*/'<ion-header no-border>\n  <ion-navbar color="comunicacao">\n    <ion-title>Comunicação Pessoal</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-card class="etapa-card" tappable (click)="navigateToPage(\'PotencialComunicativoPage\')">\n    <ion-item class="titulo">\n      <ion-grid>\n        <ion-row>\n          <ion-col>\n            <div class="left">\n              <ion-icon class="goTo" name="arrow-dropright-circle" item-left></ion-icon>\n            </div>\n          </ion-col>\n          <ion-col>\n            <div class="middle">\n              Potencial Comunicativo\n            </div>\n          </ion-col>\n          <ion-col>\n            <div class="right">\n              <ion-icon class="goTo" [name]="iconDeclaracao" item-left></ion-icon>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n  </ion-card>\n\n  <ion-card class="etapa-card" tappable (click)="navigateToPage(\'PlacarPage\')">\n    <ion-item class="titulo">\n      <ion-grid>\n        <ion-row>\n          <ion-col>\n            <div class="left">\n              <ion-icon class="goTo" name="arrow-dropright-circle" item-left></ion-icon>\n            </div>\n          </ion-col>\n          <ion-col>\n            <div class="middle">\n              Placar\n            </div>\n          </ion-col>\n          <ion-col>\n            <div class="right">\n              <ion-icon class="goTo" [name]="iconDeclaracao" item-left></ion-icon>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n  </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"D:\Projects\g8training\src\pages\comunicacao-pessoal\comunicacao-pessoal.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
], ComunicacaoPessoalPage);

//# sourceMappingURL=comunicacao-pessoal.js.map

/***/ })

});
//# sourceMappingURL=36.js.map