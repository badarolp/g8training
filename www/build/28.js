webpackJsonp([28],{

/***/ 459:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(681);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginPageModule = (function () {
    function LoginPageModule() {
    }
    return LoginPageModule;
}());
LoginPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]),
        ],
    })
], LoginPageModule);

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 681:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_users_users__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginPage = (function () {
    function LoginPage(navCtrl, usersProvider, alertCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.usersProvider = usersProvider;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.splash = true;
        this.creds = new __WEBPACK_IMPORTED_MODULE_0__providers_users_users__["a" /* UserCredentials */]();
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        setTimeout(function () {
            _this.splash = false;
        }, 4000);
    };
    LoginPage.prototype.login = function () {
        var _this = this;
        this.usersProvider.signIn(this.creds).then(function () {
        }, function (err) {
            var alert = _this.alertCtrl.create({
                title: 'Erro!',
                message: err.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    LoginPage.prototype.openReset = function () {
        var _this = this;
        var inputAlert = this.alertCtrl.create({
            title: 'Reset Password',
            inputs: [
                {
                    name: 'email',
                    placeholder: 'Email'
                }
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel'
                },
                {
                    text: 'Resetar',
                    handler: function (data) {
                        _this.resetPw(data.email);
                    }
                }
            ]
        });
        inputAlert.present();
    };
    LoginPage.prototype.resetPw = function (email) {
        var _this = this;
        this.usersProvider.resetPw(email).then(function (res) {
            var toast = _this.toastCtrl.create({
                duration: 3000,
                message: 'Sucesso! Olhe seu email para mais informações.'
            });
            toast.present();
        }, function (err) {
            var alert = _this.alertCtrl.create({
                title: 'Error',
                message: err.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    LoginPage.prototype.register = function () {
        this.navCtrl.push('RegisterPage');
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
        selector: 'page-login',template:/*ion-inline-start:"D:\Projects\g8training\src\pages\login\login.html"*/'<div id="custom-overlay" [style.display]="splash ? \'flex\': \'none\'">\n\n  <div class="flb">\n\n    <div class="Aligner-item Aligner-item--top"></div>\n\n    <img src="assets/imgs/logog8branco.png">\n\n    <div class="Aligner-item Aligner-item--bottom"></div>\n\n  </div>\n\n</div>\n\n\n\n<ion-content class="bg">\n\n\n\n  <div class="panel">\n\n    <h1>G8 Training</h1>\n\n    <ion-card>\n\n      <form (ngSubmit)="login()" #loginForm="ngForm">\n\n        <ion-list>\n\n          <ion-item>\n\n            <ion-label color="light" floating>Email</ion-label>\n\n            <ion-input type="email" name="email" [(ngModel)]="creds.email" required></ion-input>\n\n          </ion-item>\n\n          <ion-item>\n\n            <ion-label color="light" floating>Senha</ion-label>\n\n            <ion-input type="password" name="password" [(ngModel)]="creds.password" required></ion-input>\n\n          </ion-item>\n\n        </ion-list>\n\n        <button ion-button color="primary" full type="submit" [disabled]="!loginForm.form.valid">Entrar</button>\n\n      </form>\n\n      \n\n    </ion-card>\n\n  </div>\n\n  <button ion-button clear color="light" (click)="register()" full>Criar conta</button>\n\n  <!--<button ion-button clear color="light" full>Resetar Senha</button>-->\n\n\n\n</ion-content>\n\n\n\n<ion-footer>\n\n  <label>\n\n    <strong>v0.0.9</strong>\n\n  </label>\n\n</ion-footer>'/*ion-inline-end:"D:\Projects\g8training\src\pages\login\login.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_0__providers_users_users__["b" /* UsersProvider */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["q" /* ToastController */]])
], LoginPage);

//# sourceMappingURL=login.js.map

/***/ })

});
//# sourceMappingURL=28.js.map