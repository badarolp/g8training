webpackJsonp([8],{

/***/ 480:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuestionarioPageModule", function() { return QuestionarioPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__questionario__ = __webpack_require__(702);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var QuestionarioPageModule = (function () {
    function QuestionarioPageModule() {
    }
    return QuestionarioPageModule;
}());
QuestionarioPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__questionario__["a" /* QuestionarioPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__questionario__["a" /* QuestionarioPage */]),
        ],
    })
], QuestionarioPageModule);

//# sourceMappingURL=questionario.module.js.map

/***/ }),

/***/ 702:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuestionarioPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var QuestionarioPage = (function () {
    function QuestionarioPage(navCtrl, navParams, alertCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.count = 9;
        this.persona = '';
        this.showIcon = true;
        this.preload = true;
    }
    QuestionarioPage.prototype.buildAtitudes = function () {
        this.atitudes = [
            { descricao: 'AUTO CONFIANTE', perfil: 0, marcada: false },
            { descricao: 'ENÉRGICO E DOMINANTE', perfil: 0, marcada: false },
            { descricao: 'ACEITA E GOSTA DE DESAFIOS', perfil: 0, marcada: false },
            { descricao: 'COMPETITIVO E AUDACIOSO', perfil: 0, marcada: false },
            { descricao: 'DESTEMIDO E CORAJOSO', perfil: 0, marcada: false },
            //
            { descricao: 'COMUNICATIVO', perfil: 1, marcada: false },
            { descricao: 'PERSUASIVO', perfil: 1, marcada: false },
            { descricao: 'ENTUSIASMADO', perfil: 1, marcada: false },
            { descricao: 'OTIMISTA', perfil: 1, marcada: false },
            { descricao: 'FÁCIL DE RELACIONAR', perfil: 1, marcada: false },
            //
            { descricao: 'ESTÁVEL', perfil: 2, marcada: false },
            { descricao: 'PACIENTE', perfil: 2, marcada: false },
            { descricao: 'CALMO', perfil: 2, marcada: false },
            { descricao: 'TEM RITMO CONSTANTE', perfil: 2, marcada: false },
            { descricao: 'CONSERVADOR', perfil: 2, marcada: false },
            //
            { descricao: 'PRECISO', perfil: 3, marcada: false },
            { descricao: 'ATENTO AOS DETALHES', perfil: 3, marcada: false },
            { descricao: 'DILIGENTE', perfil: 3, marcada: false },
            { descricao: 'ORGANIZADO', perfil: 3, marcada: false },
            { descricao: 'RESPONSÁVEL', perfil: 3, marcada: false }
        ];
    };
    QuestionarioPage.prototype.buildValores = function () {
        this.countValores = [
            { perfil: 0, count: 0, descricao: 'Executor' },
            { perfil: 1, count: 0, descricao: 'Comunicador' },
            { perfil: 2, count: 0, descricao: 'Planejador' },
            { perfil: 3, count: 0, descricao: 'Analista' }
        ];
    };
    QuestionarioPage.prototype.updateCount = function (atitude) {
        atitude.marcada = !atitude.marcada;
        if (atitude.marcada) {
            this.count--;
        }
        else {
            this.count++;
        }
        this.updateBotao();
    };
    QuestionarioPage.prototype.isDisable = function (checkbox) {
        if (checkbox) {
            return false;
        }
        else {
            if (this.count == 0) {
                return true;
            }
            else {
                return false;
            }
        }
    };
    QuestionarioPage.prototype.updateBotao = function () {
        if (this.count == 0) {
            this.botao = "Salvar";
        }
        else if (this.count == 1) {
            this.botao = "Falta 1...";
        }
        else {
            this.botao = "Faltam " + this.count + "...";
        }
    };
    QuestionarioPage.prototype.canSalvar = function () {
        return this.count == 0;
    };
    QuestionarioPage.prototype.salvar = function () {
        this.buildValores();
        for (var _i = 0, _a = this.atitudes; _i < _a.length; _i++) {
            var atitude = _a[_i];
            if (atitude.marcada) {
                this.countValores[atitude.perfil].count++;
            }
        }
        this.countValores.sort(function (a, b) {
            if (a.count < b.count) {
                return 1;
            }
            if (a.count > b.count) {
                return -1;
            }
            return 0;
        });
        if ((this.countValores[0].count - this.countValores[1].count) >= 2) {
            this.persona = this.countValores[0].descricao;
        }
        else {
            this.persona = this.countValores[0].descricao + ' e ' + this.countValores[1].descricao;
        }
        var alert = this.alertCtrl.create({
            title: 'Você é,',
            subTitle: this.persona + '!',
            buttons: ['Ok']
        });
        alert.present();
    };
    QuestionarioPage.prototype.ionViewDidLoad = function () {
        this.expand();
        this.buildAtitudes();
        this.buildValores();
        this.updateBotao();
    };
    QuestionarioPage.prototype.expand = function () {
        var _this = this;
        this.expanded = true;
        this.contracted = !this.expanded;
        this.showIcon = false;
        setTimeout(function () {
            var modal = _this.modalCtrl.create('ModalQuestionarioPage');
            modal.onDidDismiss(function (data) {
                _this.expanded = false;
                _this.contracted = !_this.expanded;
                setTimeout(function () { return _this.showIcon = true; }, 330);
            });
            modal.present();
        }, 200);
    };
    return QuestionarioPage;
}());
QuestionarioPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-questionario',template:/*ion-inline-start:"D:\Projects\g8training\src\pages\questionario\questionario.html"*/'<ion-header no-border>\n\n  <ion-navbar color="perfil">\n\n    <ion-title>Questionário</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n  <ion-list>\n\n    <ion-item *ngFor="let a of atitudes">\n\n      <ion-label>{{a.descricao}}</ion-label>\n\n      <ion-checkbox [disabled]="isDisable(a.marcada)" (ionChange)="updateCount(a)" color="perfil"></ion-checkbox>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n</ion-content>\n\n\n\n<ion-footer>\n\n  <button [disabled]="!canSalvar()" full ion-button color="perfil" (click)="salvar()">{{botao}}</button>\n\n</ion-footer>\n\n\n\n'/*ion-inline-end:"D:\Projects\g8training\src\pages\questionario\questionario.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */]])
], QuestionarioPage);

//# sourceMappingURL=questionario.js.map

/***/ })

});
//# sourceMappingURL=8.js.map