webpackJsonp([9],{

/***/ 479:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PotencialComunicativoPageModule", function() { return PotencialComunicativoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__potencial_comunicativo__ = __webpack_require__(701);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PotencialComunicativoPageModule = (function () {
    function PotencialComunicativoPageModule() {
    }
    return PotencialComunicativoPageModule;
}());
PotencialComunicativoPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__potencial_comunicativo__["a" /* PotencialComunicativoPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__potencial_comunicativo__["a" /* PotencialComunicativoPage */]),
        ],
    })
], PotencialComunicativoPageModule);

//# sourceMappingURL=potencial-comunicativo.module.js.map

/***/ }),

/***/ 701:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PotencialComunicativoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PotencialComunicativoPage = (function () {
    function PotencialComunicativoPage(navCtrl, navParams, alertCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.count = 20;
        this.persona = '';
        this.showIcon = true;
        this.preload = true;
    }
    PotencialComunicativoPage.prototype.buildAtitudes = function () {
        this.atitudes1 = [
            { descricao: 'Trovejar', perfil: 1, marcada: false },
            { descricao: 'Retrato', perfil: 0, marcada: false },
            { descricao: 'Mordida', perfil: 2, marcada: false },
            { descricao: 'Desafinado', perfil: 1, marcada: false },
            { descricao: 'Auréola', perfil: 0, marcada: false },
            { descricao: 'Mexer', perfil: 2, marcada: false },
            { descricao: 'Emoção', perfil: 2, marcada: false },
            { descricao: 'Trombeta', perfil: 1, marcada: false },
            { descricao: 'Aparência', perfil: 0, marcada: false },
            { descricao: 'Miragem', perfil: 0, marcada: false },
            { descricao: 'Grunhido', perfil: 1, marcada: false },
            { descricao: 'Ventania', perfil: 2, marcada: false },
            { descricao: 'Conforto', perfil: 2, marcada: false },
            { descricao: 'Audiência', perfil: 1, marcada: false },
            { descricao: 'Desbotado', perfil: 0, marcada: false },
            { descricao: 'Coceira', perfil: 2, marcada: false },
            { descricao: 'Ruborizar', perfil: 0, marcada: false },
            { descricao: 'Palpável', perfil: 2, marcada: false },
            { descricao: 'Iluminação', perfil: 0, marcada: false },
            { descricao: 'Doçura', perfil: 2, marcada: false },
            { descricao: 'Eco', perfil: 1, marcada: false },
            { descricao: 'Transparecer', perfil: 0, marcada: false },
            { descricao: 'Timbre', perfil: 1, marcada: false },
            { descricao: 'Focalizar', perfil: 0, marcada: false },
            { descricao: 'Perfume', perfil: 2, marcada: false },
            { descricao: 'Ofuscar', perfil: 0, marcada: false },
            { descricao: 'Barulho', perfil: 1, marcada: false },
            { descricao: 'Panorama', perfil: 0, marcada: false },
            { descricao: 'Eloquência', perfil: 1, marcada: false },
            { descricao: 'Periscópio', perfil: 0, marcada: false },
            { descricao: 'Assobio', perfil: 1, marcada: false },
            { descricao: 'Colorir', perfil: 0, marcada: false },
            { descricao: 'Campainha', perfil: 1, marcada: false }
        ];
        this.atitudes2 = [
            { descricao: 'Mergulhar', perfil: 2, marcada: false },
            { descricao: 'Discurso', perfil: 1, marcada: false },
            { descricao: 'Queimadura', perfil: 2, marcada: false },
            { descricao: 'Murmurar', perfil: 1, marcada: false },
            { descricao: 'Saboroso', perfil: 2, marcada: false },
            { descricao: 'Gesticular', perfil: 2, marcada: false },
            { descricao: 'Espinho', perfil: 2, marcada: false },
            { descricao: 'Estampa', perfil: 0, marcada: false },
            { descricao: 'Sensação', perfil: 2, marcada: false },
            { descricao: 'Sotaque', perfil: 1, marcada: false },
            { descricao: 'Visualização', perfil: 0, marcada: false },
            { descricao: 'Aroma', perfil: 2, marcada: false },
            { descricao: 'Ritmo', perfil: 2, marcada: false },
            { descricao: 'Retórica', perfil: 1, marcada: false },
            { descricao: 'Gorjeio', perfil: 1, marcada: false },
            { descricao: 'Sintonizar', perfil: 1, marcada: false },
            { descricao: 'Áspero', perfil: 2, marcada: false },
            { descricao: 'Pálido', perfil: 0, marcada: false },
            { descricao: 'Vozerio', perfil: 1, marcada: false },
            { descricao: 'Veludo', perfil: 2, marcada: false },
            { descricao: 'Claridade', perfil: 0, marcada: false },
            { descricao: 'Observar', perfil: 0, marcada: false },
            { descricao: 'Silêncio', perfil: 1, marcada: false },
            { descricao: 'Arrancar', perfil: 2, marcada: false },
            { descricao: 'Brilhante', perfil: 0, marcada: false },
            { descricao: 'Orquestra', perfil: 1, marcada: false },
            { descricao: 'Paisagem', perfil: 0, marcada: false },
            { descricao: 'Textura', perfil: 2, marcada: false },
            { descricao: 'Acústico', perfil: 1, marcada: false },
            { descricao: 'Segurar', perfil: 2, marcada: false },
            { descricao: 'Espelho', perfil: 0, marcada: false },
            { descricao: 'Sinfonia', perfil: 1, marcada: false },
            { descricao: 'Cenário', perfil: 0, marcada: false }
        ];
    };
    PotencialComunicativoPage.prototype.buildValores = function () {
        this.countValores = [
            { perfil: 0, count: 0, descricao: 'Visual' },
            { perfil: 1, count: 0, descricao: 'Auditivo' },
            { perfil: 2, count: 0, descricao: 'Cinestésico' }
        ];
    };
    PotencialComunicativoPage.prototype.updateCount = function (atitude) {
        atitude.marcada = !atitude.marcada;
        if (atitude.marcada) {
            this.count--;
        }
        else {
            this.count++;
        }
        this.updateBotao();
    };
    PotencialComunicativoPage.prototype.isDisable = function (checkbox) {
        if (checkbox) {
            return false;
        }
        else {
            if (this.count == 0) {
                return true;
            }
            else {
                return false;
            }
        }
    };
    PotencialComunicativoPage.prototype.updateBotao = function () {
        if (this.count == 0) {
            this.botao = "Salvar";
        }
        else if (this.count == 1) {
            this.botao = "Falta 1...";
        }
        else {
            this.botao = "Faltam " + this.count + "...";
        }
    };
    PotencialComunicativoPage.prototype.canSalvar = function () {
        return this.count == 0;
    };
    PotencialComunicativoPage.prototype.salvar = function () {
        this.buildValores();
        for (var _i = 0, _a = this.atitudes1; _i < _a.length; _i++) {
            var atitude = _a[_i];
            if (atitude.marcada) {
                this.countValores[atitude.perfil].count++;
            }
        }
        for (var _b = 0, _c = this.atitudes2; _b < _c.length; _b++) {
            var atitude = _c[_b];
            if (atitude.marcada) {
                this.countValores[atitude.perfil].count++;
            }
        }
        this.countValores.sort(function (a, b) {
            if (a.count < b.count) {
                return 1;
            }
            if (a.count > b.count) {
                return -1;
            }
            return 0;
        });
        if ((this.countValores[0].count - this.countValores[1].count) >= 2) {
            this.persona = this.countValores[0].descricao;
        }
        else {
            this.persona = this.countValores[0].descricao + ' e ' + this.countValores[1].descricao;
        }
        var alert = this.alertCtrl.create({
            title: 'Você é,',
            subTitle: this.persona + '!',
            buttons: ['Ok']
        });
        alert.present();
    };
    PotencialComunicativoPage.prototype.ionViewDidLoad = function () {
        this.expand();
        this.buildAtitudes();
        this.buildValores();
        this.updateBotao();
    };
    PotencialComunicativoPage.prototype.expand = function () {
        var _this = this;
        this.expanded = true;
        this.contracted = !this.expanded;
        this.showIcon = false;
        setTimeout(function () {
            var modal = _this.modalCtrl.create('SlideWalkthroughPage');
            modal.onDidDismiss(function (data) {
                _this.expanded = false;
                _this.contracted = !_this.expanded;
                setTimeout(function () { return _this.showIcon = true; }, 330);
            });
            modal.present();
        }, 200);
    };
    return PotencialComunicativoPage;
}());
PotencialComunicativoPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-potencial-comunicativo',template:/*ion-inline-start:"D:\Projects\g8training\src\pages\potencial-comunicativo\potencial-comunicativo.html"*/'<ion-header no-border>\n  <ion-navbar color="comunicacao">\n    <ion-title>Potencial Comunicativo</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-grid>\n    <ion-row>\n\n      <ion-col>\n        <ion-item *ngFor="let atitude of atitudes1">\n          <ion-label>{{atitude.descricao}}</ion-label>\n          <ion-checkbox [disabled]="isDisable(atitude.marcada)" (ionChange)="updateCount(atitude)" color="comunicacao"></ion-checkbox>\n        </ion-item>\n      </ion-col>\n\n      <ion-col>\n        <ion-item *ngFor="let atitude of atitudes2">\n          <ion-label>{{atitude.descricao}}</ion-label>\n          <ion-checkbox [disabled]="isDisable(atitude.marcada)" (ionChange)="updateCount(atitude)" color="comunicacao"></ion-checkbox>\n        </ion-item>\n      </ion-col>\n\n    </ion-row>\n  </ion-grid>\n\n</ion-content>\n\n<ion-footer>\n  <button [disabled]="!canSalvar()" full ion-button color="comunicacao" (click)="salvar()">{{botao}}</button>\n</ion-footer>'/*ion-inline-end:"D:\Projects\g8training\src\pages\potencial-comunicativo\potencial-comunicativo.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */]])
], PotencialComunicativoPage);

//# sourceMappingURL=potencial-comunicativo.js.map

/***/ })

});
//# sourceMappingURL=9.js.map