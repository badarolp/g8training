webpackJsonp([29],{

/***/ 458:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JogoDosNumerosPageModule", function() { return JogoDosNumerosPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__jogo_dos_numeros__ = __webpack_require__(680);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var JogoDosNumerosPageModule = (function () {
    function JogoDosNumerosPageModule() {
    }
    return JogoDosNumerosPageModule;
}());
JogoDosNumerosPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__jogo_dos_numeros__["a" /* JogoDosNumerosPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__jogo_dos_numeros__["a" /* JogoDosNumerosPage */])
        ],
    })
], JogoDosNumerosPageModule);

//# sourceMappingURL=jogo-dos-numeros.module.js.map

/***/ }),

/***/ 680:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JogoDosNumerosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var JogoDosNumerosPage = (function () {
    function JogoDosNumerosPage(navCtrl, navParams, modalCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.timeInSeconds = 60;
        this.pontos = 0;
        this.tempoEmSegundos = 60;
        this.showIcon = true;
        this.preload = true;
        this.pressedColor = '#ADFF2F';
        this.wrongColor = '#FF5151';
        this.defaultColor = '#D1D4F6';
        this.degreeStyle10 = 'rotate(10deg)';
        this.degreeStyle20 = 'rotate(20deg)';
        this.degreeStyle30 = 'rotate(30deg)';
        this.degreeStyle40 = 'rotate(40deg)';
        this.buttonColor1 = '#D1D4F6';
        this.buttonColor2 = '#D1D4F6';
        this.buttonColor3 = '#D1D4F6';
        this.buttonColor4 = '#D1D4F6';
        this.buttonColor5 = '#D1D4F6';
        this.buttonColor6 = '#D1D4F6';
        this.buttonColor7 = '#D1D4F6';
        this.buttonColor8 = '#D1D4F6';
        this.buttonColor9 = '#D1D4F6';
        this.buttonColor10 = '#D1D4F6';
        this.buttonColor11 = '#D1D4F6';
        this.buttonColor12 = '#D1D4F6';
        this.buttonColor13 = '#D1D4F6';
        this.buttonColor14 = '#D1D4F6';
        this.buttonColor15 = '#D1D4F6';
        this.buttonColor16 = '#D1D4F6';
        this.buttonColor17 = '#D1D4F6';
        this.buttonColor18 = '#D1D4F6';
        this.buttonColor19 = '#D1D4F6';
        this.buttonColor20 = '#D1D4F6';
        this.buttonColor21 = '#D1D4F6';
        this.buttonColor22 = '#D1D4F6';
        this.buttonColor23 = '#D1D4F6';
        this.buttonColor24 = '#D1D4F6';
        this.buttonColor25 = '#D1D4F6';
        this.buttonColor26 = '#D1D4F6';
        this.buttonColor27 = '#D1D4F6';
        this.buttonColor28 = '#D1D4F6';
        this.buttonColor29 = '#D1D4F6';
        this.buttonColor30 = '#D1D4F6';
        this.buttonColor31 = '#D1D4F6';
        this.buttonColor32 = '#D1D4F6';
        this.buttonColor33 = '#D1D4F6';
        this.buttonColor34 = '#D1D4F6';
        this.buttonColor35 = '#D1D4F6';
        this.buttonColor36 = '#D1D4F6';
        this.buttonColor37 = '#D1D4F6';
        this.buttonColor38 = '#D1D4F6';
        this.buttonColor39 = '#D1D4F6';
        this.buttonColor40 = '#D1D4F6';
        this.buttonColor41 = '#D1D4F6';
        this.buttonColor42 = '#D1D4F6';
        this.buttonColor43 = '#D1D4F6';
        this.buttonColor44 = '#D1D4F6';
        this.buttonColor45 = '#D1D4F6';
        this.buttonColor46 = '#D1D4F6';
        this.buttonColor47 = '#D1D4F6';
        this.buttonColor48 = '#D1D4F6';
        this.buttonColor49 = '#D1D4F6';
        this.buttonColor50 = '#D1D4F6';
        //buttoAtual: string;
        this.numeroAtual = 1;
    }
    JogoDosNumerosPage.prototype.navigateToPage = function (pageName) {
        this.navCtrl.push(pageName);
    };
    JogoDosNumerosPage.prototype.navigateToBack = function () {
        this.navCtrl.pop();
    };
    JogoDosNumerosPage.prototype.paintButton = function (button) {
        var vColor;
        var vOk = this.numeroAtual == Number(button);
        if (vOk) {
            vColor = this.pressedColor;
            if (this.numeroAtual == 50) {
                this.terminar();
            }
            else {
                this.numeroAtual++;
                this.botaoCerto();
            }
        }
        else {
            vColor = this.wrongColor;
            this.botaoErrado();
        }
        switch (button) {
            case ("1"): {
                this.buttonColor1 = vColor;
                break;
            }
            case ("2"): {
                this.buttonColor2 = vColor;
                break;
            }
            case ("3"): {
                this.buttonColor3 = vColor;
                break;
            }
            case ("4"): {
                this.buttonColor4 = vColor;
                break;
            }
            case ("5"): {
                this.buttonColor5 = vColor;
                break;
            }
            case ("6"): {
                this.buttonColor6 = vColor;
                break;
            }
            case ("7"): {
                this.buttonColor7 = vColor;
                break;
            }
            case ("8"): {
                this.buttonColor8 = vColor;
                break;
            }
            case ("9"): {
                this.buttonColor9 = vColor;
                break;
            }
            case ("10"): {
                this.buttonColor10 = vColor;
                break;
            }
            case ("11"): {
                this.buttonColor11 = vColor;
                break;
            }
            case ("12"): {
                this.buttonColor12 = vColor;
                break;
            }
            case ("13"): {
                this.buttonColor13 = vColor;
                break;
            }
            case ("14"): {
                this.buttonColor14 = vColor;
                break;
            }
            case ("15"): {
                this.buttonColor15 = vColor;
                break;
            }
            case ("16"): {
                this.buttonColor16 = vColor;
                break;
            }
            case ("17"): {
                this.buttonColor17 = vColor;
                break;
            }
            case ("18"): {
                this.buttonColor18 = vColor;
                break;
            }
            case ("19"): {
                this.buttonColor19 = vColor;
                break;
            }
            case ("20"): {
                this.buttonColor20 = vColor;
                break;
            }
            case ("21"): {
                this.buttonColor21 = vColor;
                break;
            }
            case ("22"): {
                this.buttonColor22 = vColor;
                break;
            }
            case ("23"): {
                this.buttonColor23 = vColor;
                break;
            }
            case ("24"): {
                this.buttonColor24 = vColor;
                break;
            }
            case ("25"): {
                this.buttonColor25 = vColor;
                break;
            }
            case ("26"): {
                this.buttonColor26 = vColor;
                break;
            }
            case ("27"): {
                this.buttonColor27 = vColor;
                break;
            }
            case ("28"): {
                this.buttonColor28 = vColor;
                break;
            }
            case ("29"): {
                this.buttonColor29 = vColor;
                break;
            }
            case ("30"): {
                this.buttonColor30 = vColor;
                break;
            }
            case ("31"): {
                this.buttonColor31 = vColor;
                break;
            }
            case ("32"): {
                this.buttonColor32 = vColor;
                break;
            }
            case ("33"): {
                this.buttonColor33 = vColor;
                break;
            }
            case ("34"): {
                this.buttonColor34 = vColor;
                break;
            }
            case ("35"): {
                this.buttonColor35 = vColor;
                break;
            }
            case ("36"): {
                this.buttonColor36 = vColor;
                break;
            }
            case ("37"): {
                this.buttonColor37 = vColor;
                break;
            }
            case ("38"): {
                this.buttonColor38 = vColor;
                break;
            }
            case ("39"): {
                this.buttonColor39 = vColor;
                break;
            }
            case ("40"): {
                this.buttonColor40 = vColor;
                break;
            }
            case ("41"): {
                this.buttonColor41 = vColor;
                break;
            }
            case ("42"): {
                this.buttonColor42 = vColor;
                break;
            }
            case ("43"): {
                this.buttonColor43 = vColor;
                break;
            }
            case ("44"): {
                this.buttonColor44 = vColor;
                break;
            }
            case ("45"): {
                this.buttonColor45 = vColor;
                break;
            }
            case ("46"): {
                this.buttonColor46 = vColor;
                break;
            }
            case ("47"): {
                this.buttonColor47 = vColor;
                break;
            }
            case ("48"): {
                this.buttonColor48 = vColor;
                break;
            }
            case ("49"): {
                this.buttonColor49 = vColor;
                break;
            }
            case ("50"): {
                this.buttonColor50 = vColor;
                break;
            }
        }
    };
    JogoDosNumerosPage.prototype.press = function (button) {
        if (this.started) {
            this.paintButton(button);
        }
    };
    JogoDosNumerosPage.prototype.expand = function () {
        var _this = this;
        this.expanded = true;
        this.contracted = !this.expanded;
        this.showIcon = false;
        setTimeout(function () {
            var modal = _this.modalCtrl.create('ModalJogoDosNumerosPage');
            modal.onDidDismiss(function (data) {
                _this.expanded = false;
                _this.contracted = !_this.expanded;
                setTimeout(function () { return _this.showIcon = true; }, 330);
            });
            modal.present();
        }, 200);
    };
    JogoDosNumerosPage.prototype.ngOnInit = function () {
        this.initTimer();
    };
    JogoDosNumerosPage.prototype.hasFinished = function () {
        return this.finished;
    };
    JogoDosNumerosPage.prototype.initTimer = function () {
        if (!this.timeInSeconds) {
            this.timeInSeconds = 0;
        }
        this.seconds = this.timeInSeconds;
        this.runTimer = false;
        this.started = false;
        this.finished = false;
        this.secondsRemaining = this.timeInSeconds;
        this.displayTime = this.getSecondsAsDigitalClock(this.secondsRemaining);
    };
    JogoDosNumerosPage.prototype.startTimer = function () {
        this.initGame();
        this.started = true;
        this.runTimer = true;
        this.timerTick();
    };
    JogoDosNumerosPage.prototype.pauseTimer = function () {
        this.runTimer = false;
    };
    JogoDosNumerosPage.prototype.resumeTimer = function () {
        this.startTimer();
    };
    JogoDosNumerosPage.prototype.timerTick = function () {
        var _this = this;
        setTimeout(function () {
            if (!_this.runTimer) {
                return;
            }
            _this.secondsRemaining--;
            _this.displayTime = _this.getSecondsAsDigitalClock(_this.secondsRemaining);
            if (_this.secondsRemaining > 0) {
                _this.timerTick();
            }
            else {
                _this.finished = true;
                _this.terminar();
            }
        }, 1000);
    };
    JogoDosNumerosPage.prototype.getSecondsAsDigitalClock = function (inputSeconds) {
        var secNum = parseInt(inputSeconds.toString(), 10); // don't forget the second param
        var hours = Math.floor(secNum / 3600);
        var minutes = Math.floor((secNum - (hours * 3600)) / 60);
        var seconds = secNum - (hours * 3600) - (minutes * 60);
        var minutesString = '';
        var secondsString = '';
        minutesString = (minutes < 10) ? '0' + minutes : minutes.toString();
        secondsString = (seconds < 10) ? '0' + seconds : seconds.toString();
        return minutesString + ':' + secondsString;
    };
    JogoDosNumerosPage.prototype.botaoCerto = function () {
        this.pontos = this.pontos + 3;
    };
    JogoDosNumerosPage.prototype.botaoErrado = function () {
        var _this = this;
        this.pontos = this.pontos - 20 + this.secondsRemaining;
        var alert = this.alertCtrl.create({
            title: 'Botão errado!',
            subTitle: 'Sua pontuação é ' + this.pontos,
            buttons: [
                {
                    text: 'OK',
                    handler: function () {
                        _this.initGame();
                    }
                }
            ]
        });
        alert.present();
        this.pontos = 0;
        this.initTimer();
    };
    JogoDosNumerosPage.prototype.terminar = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Acabou o tempo!',
            subTitle: 'Sua pontuação é ' + this.pontos,
            buttons: [
                {
                    text: 'OK',
                    handler: function () {
                        _this.initGame();
                    }
                }
            ]
        });
        alert.present();
        this.pontos = 0;
        this.initTimer();
    };
    JogoDosNumerosPage.prototype.initGame = function () {
        this.numeroAtual = 1;
        this.pontos = 0;
        this.buttonColor1 = '#D1D4F6';
        this.buttonColor2 = '#D1D4F6';
        this.buttonColor3 = '#D1D4F6';
        this.buttonColor4 = '#D1D4F6';
        this.buttonColor5 = '#D1D4F6';
        this.buttonColor6 = '#D1D4F6';
        this.buttonColor7 = '#D1D4F6';
        this.buttonColor8 = '#D1D4F6';
        this.buttonColor9 = '#D1D4F6';
        this.buttonColor10 = '#D1D4F6';
        this.buttonColor11 = '#D1D4F6';
        this.buttonColor12 = '#D1D4F6';
        this.buttonColor13 = '#D1D4F6';
        this.buttonColor14 = '#D1D4F6';
        this.buttonColor15 = '#D1D4F6';
        this.buttonColor16 = '#D1D4F6';
        this.buttonColor17 = '#D1D4F6';
        this.buttonColor18 = '#D1D4F6';
        this.buttonColor19 = '#D1D4F6';
        this.buttonColor20 = '#D1D4F6';
        this.buttonColor21 = '#D1D4F6';
        this.buttonColor22 = '#D1D4F6';
        this.buttonColor23 = '#D1D4F6';
        this.buttonColor24 = '#D1D4F6';
        this.buttonColor25 = '#D1D4F6';
        this.buttonColor26 = '#D1D4F6';
        this.buttonColor27 = '#D1D4F6';
        this.buttonColor28 = '#D1D4F6';
        this.buttonColor29 = '#D1D4F6';
        this.buttonColor30 = '#D1D4F6';
        this.buttonColor31 = '#D1D4F6';
        this.buttonColor32 = '#D1D4F6';
        this.buttonColor33 = '#D1D4F6';
        this.buttonColor34 = '#D1D4F6';
        this.buttonColor35 = '#D1D4F6';
        this.buttonColor36 = '#D1D4F6';
        this.buttonColor37 = '#D1D4F6';
        this.buttonColor38 = '#D1D4F6';
        this.buttonColor39 = '#D1D4F6';
        this.buttonColor40 = '#D1D4F6';
        this.buttonColor41 = '#D1D4F6';
        this.buttonColor42 = '#D1D4F6';
        this.buttonColor43 = '#D1D4F6';
        this.buttonColor44 = '#D1D4F6';
        this.buttonColor45 = '#D1D4F6';
        this.buttonColor46 = '#D1D4F6';
        this.buttonColor47 = '#D1D4F6';
        this.buttonColor48 = '#D1D4F6';
        this.buttonColor49 = '#D1D4F6';
        this.buttonColor50 = '#D1D4F6';
    };
    JogoDosNumerosPage.prototype.ionViewCanLeave = function () {
        this.initTimer();
    };
    JogoDosNumerosPage.prototype.ionViewCanEnter = function () {
        this.expand();
    };
    return JogoDosNumerosPage;
}());
JogoDosNumerosPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-jogo-dos-numeros',template:/*ion-inline-start:"D:\Projects\g8training\src\pages\jogo-dos-numeros\jogo-dos-numeros.html"*/'<ion-header>\n\n    <ion-navbar color="tempo">\n\n      <ion-title>Jogo dos Números</ion-title>  \n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n  <ion-grid>\n\n\n\n    <ion-row>\n\n\n\n      <ion-col>\n\n\n\n        <ion-row>\n\n            <ion-col no-padding>\n\n\n\n            </ion-col>\n\n            <ion-col no-padding>\n\n\n\n            </ion-col>\n\n            <ion-col no-padding>\n\n              <button class="numero" (click)="press(\'45\');" [ngStyle]="{\'background-color\': buttonColor45}">\n\n                <label><b>45</b></label>\n\n              </button>\n\n            </ion-col>          \n\n            <ion-col no-padding>\n\n\n\n            </ion-col>                    \n\n          </ion-row>        \n\n\n\n        <ion-row>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'49\');" [ngStyle]="{\'background-color\': buttonColor49}">\n\n              <label><b>49</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'21\');" [ngStyle]="{\'background-color\': buttonColor21}">\n\n              <label><b>21</b></label>\n\n            </button>\n\n          </ion-col>          \n\n          <ion-col no-padding>\n\n\n\n          </ion-col>                    \n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'29\');" [ngStyle]="{\'background-color\': buttonColor29}">\n\n              <label><b>29</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'1\');" [ngStyle]="{\'background-color\': buttonColor1}">\n\n              <label><b>1</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>          \n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'37\');" [ngStyle]="{\'background-color\': buttonColor37}">\n\n              <label><b>37</b></label>\n\n            </button>\n\n          </ion-col>                    \n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col no-padding>\n\n\n\n          </ion-col>                    \n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'33\');" [ngStyle]="{\'background-color\': buttonColor33}">\n\n              <label><b>33</b></label>\n\n            </button>            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'5\');" [ngStyle]="{\'background-color\': buttonColor5}">\n\n              <label><b>5</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n\n\n          </ion-col>                    \n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'13\');" [ngStyle]="{\'background-color\': buttonColor13}">\n\n              <label><b>13</b></label>\n\n            </button>\n\n          </ion-col>                    \n\n          <ion-col no-padding>\n\n\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'25\');" [ngStyle]="{\'background-color\': buttonColor25}">\n\n              <label><b>25</b></label>\n\n            </button>\n\n          </ion-col>                    \n\n        </ion-row>\n\n        \n\n        <ion-row>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>                    \n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'17\');" [ngStyle]="{\'background-color\': buttonColor17}">\n\n              <label><b>17</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'41\');" [ngStyle]="{\'background-color\': buttonColor41}">\n\n              <label>\n\n                <b>41</b>\n\n              </label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'9\');" [ngStyle]="{\'background-color\': buttonColor9}">\n\n              <label>\n\n                <b>9</b>\n\n              </label>\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>        \n\n\n\n        <ion-row>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n\n\n          </ion-col>          \n\n          <ion-col no-padding>\n\n\n\n          </ion-col>\n\n        </ion-row>\n\n\n\n      </ion-col>\n\n\n\n      <ion-col>\n\n\n\n        <ion-row>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'22\');" [ngStyle]="{\'background-color\': buttonColor22}">\n\n              <label><b>22</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'38\');" [ngStyle]="{\'background-color\': buttonColor38}">\n\n              <label>\n\n                <b>38</b>\n\n              </label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'10\');" [ngStyle]="{\'background-color\': buttonColor10}">\n\n              <label><b>10</b></label>\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col no-padding>\n\n\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'26\');" [ngStyle]="{\'background-color\': buttonColor26}">\n\n              <label><b>26</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'42\');" [ngStyle]="{\'background-color\': buttonColor42}">\n\n              <label><b>42</b></label>\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'30\');" [ngStyle]="{\'background-color\': buttonColor30}">\n\n              <label><b>30</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'50\');" [ngStyle]="{\'background-color\': buttonColor50}">\n\n              <label><b>50</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'6\');" [ngStyle]="{\'background-color\': buttonColor6}">\n\n              <label><b>6</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'2\');" [ngStyle]="{\'background-color\': buttonColor2}">\n\n              <label><b>2</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'14\');" [ngStyle]="{\'background-color\': buttonColor14}">\n\n              <label><b>14</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'46\');" [ngStyle]="{\'background-color\': buttonColor46}">\n\n              <label><b>46</b></label>\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'18\');" [ngStyle]="{\'background-color\': buttonColor18}">\n\n              <label>\n\n                <b>18</b>\n\n              </label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'34\');" [ngStyle]="{\'background-color\': buttonColor34}">\n\n              <label>\n\n                <b>34</b>\n\n              </label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n        </ion-row>\n\n        \n\n        <ion-row>\n\n          <ion-col no-padding>\n\n\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n\n\n          </ion-col>\n\n        </ion-row>        \n\n\n\n      </ion-col>      \n\n    \n\n    </ion-row>\n\n\n\n    <ion-row>\n\n        \n\n      <ion-col>\n\n\n\n        <ion-row>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'7\');" [ngStyle]="{\'background-color\': buttonColor7}">\n\n              <label><b>7</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'43\');" [ngStyle]="{\'background-color\': buttonColor43}">\n\n              <label><b>43</b></label>\n\n            </button>\n\n          </ion-col>          \n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>                    \n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'23\');" [ngStyle]="{\'background-color\': buttonColor23}">\n\n              <label><b>23</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>          \n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'31\');" [ngStyle]="{\'background-color\': buttonColor31}">\n\n              <label><b>31</b></label>\n\n            </button>\n\n          </ion-col>                    \n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'19\');" [ngStyle]="{\'background-color\': buttonColor19}">\n\n              <label><b>19</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'35\');" [ngStyle]="{\'background-color\': buttonColor35}">\n\n              <label><b>35</b></label>\n\n            </button>\n\n          </ion-col>          \n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>                    \n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'3\');" [ngStyle]="{\'background-color\': buttonColor3}">\n\n              <label><b>3</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>          \n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'27\');" [ngStyle]="{\'background-color\': buttonColor27}">\n\n              <label><b>27</b></label>\n\n            </button>\n\n          </ion-col>                    \n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'47\');" [ngStyle]="{\'background-color\': buttonColor47}">\n\n              <label><b>47</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>          \n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'39\');" [ngStyle]="{\'background-color\': buttonColor39}">\n\n              <label><b>39</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'11\');" [ngStyle]="{\'background-color\': buttonColor11}">\n\n              <label>\n\n                <b>11</b>\n\n              </label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'15\');" [ngStyle]="{\'background-color\': buttonColor15}">\n\n              <label><b>15</b></label>\n\n            </button>\n\n          </ion-col>          \n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n        </ion-row>\n\n        \n\n        <ion-row>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>          \n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n        </ion-row>        \n\n\n\n      </ion-col>\n\n\n\n      <ion-col>\n\n\n\n        <ion-row>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'40\');" [ngStyle]="{\'background-color\': buttonColor40}">\n\n              <label><b>40</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'32\');" [ngStyle]="{\'background-color\': buttonColor32}">\n\n              <label><b>32</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'8\');" [ngStyle]="{\'background-color\': buttonColor8}">\n\n              <label><b>8</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'16\');" [ngStyle]="{\'background-color\': buttonColor16}">\n\n              <label><b>16</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'12\');" [ngStyle]="{\'background-color\': buttonColor12}">\n\n              <label><b>12</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'44\');" [ngStyle]="{\'background-color\': buttonColor44}">\n\n              <label><b>44</b></label>\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'4\');" [ngStyle]="{\'background-color\': buttonColor4}">\n\n              <label><b>4</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'36\');" [ngStyle]="{\'background-color\': buttonColor36}">\n\n              <label><b>36</b></label>\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'24\');" [ngStyle]="{\'background-color\': buttonColor24}">\n\n              <label><b>24</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n        </ion-row>\n\n        \n\n        <ion-row>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'20\');" [ngStyle]="{\'background-color\': buttonColor20}">\n\n              <label><b>20</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'48\');" [ngStyle]="{\'background-color\': buttonColor48}">\n\n              <label><b>48</b></label>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button class="numero" (click)="press(\'28\');" [ngStyle]="{\'background-color\': buttonColor28}">\n\n              <label>\n\n                <b>28</b>\n\n              </label>\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n        \n\n        <ion-row>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            \n\n          </ion-col>\n\n          <ion-col no-padding>\n\n\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n\n\n          </ion-col>\n\n        </ion-row>        \n\n\n\n      </ion-col>      \n\n    \n\n    </ion-row>    \n\n\n\n  </ion-grid>\n\n\n\n</ion-content>\n\n\n\n<ion-footer>\n\n  <ion-toolbar>\n\n    <div *ngIf="!started">\n\n      <button padding ion-button full color="tempo" (click)=\'startTimer()\'>Iniciar</button>\n\n    </div>\n\n    <div *ngIf="started">\n\n      <ion-row>\n\n        <ion-col no-padding>\n\n          <div no-padding>\n\n            <button ion-button large block clear color="tempo" class="timer-button x-large">{{displayTime}}</button>\n\n          </div>\n\n        </ion-col>\n\n        <ion-col no-padding>\n\n          <div no-padding>\n\n            <button ion-button large block clear color="tempo" class="timer-button x-large">{{pontos}} PONTOS</button>\n\n          </div>\n\n        </ion-col>\n\n      </ion-row>\n\n    </div>\n\n  </ion-toolbar>\n\n</ion-footer>\n\n\n\n'/*ion-inline-end:"D:\Projects\g8training\src\pages\jogo-dos-numeros\jogo-dos-numeros.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
], JogoDosNumerosPage);

//# sourceMappingURL=jogo-dos-numeros.js.map

/***/ })

});
//# sourceMappingURL=29.js.map