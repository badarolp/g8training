webpackJsonp([20],{

/***/ 467:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MetaPrincipalPageModule", function() { return MetaPrincipalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__meta_principal__ = __webpack_require__(689);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MetaPrincipalPageModule = (function () {
    function MetaPrincipalPageModule() {
    }
    return MetaPrincipalPageModule;
}());
MetaPrincipalPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__meta_principal__["a" /* MetaPrincipalPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__meta_principal__["a" /* MetaPrincipalPage */]),
        ],
    })
], MetaPrincipalPageModule);

//# sourceMappingURL=meta-principal.module.js.map

/***/ }),

/***/ 689:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MetaPrincipalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_users_users__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MetaPrincipalPage = (function () {
    function MetaPrincipalPage(navCtrl, usersProvider, toastCtrl) {
        this.navCtrl = navCtrl;
        this.usersProvider = usersProvider;
        this.toastCtrl = toastCtrl;
    }
    MetaPrincipalPage.prototype.editarMeta = function () {
        var _this = this;
        this.usersProvider.updateMeta(this.meta).then(function () {
            var toast = _this.toastCtrl.create({
                duration: 3000,
                message: 'Meta editada!'
            });
            toast.present();
        });
    };
    MetaPrincipalPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.metaSubscription = this.usersProvider.getCurrentMeta().subscribe(function (meta) {
            _this.meta = meta;
        });
    };
    MetaPrincipalPage.prototype.navigateToBack = function () {
        this.navCtrl.pop();
    };
    return MetaPrincipalPage;
}());
MetaPrincipalPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
        selector: 'page-meta-principal',template:/*ion-inline-start:"D:\Projects\g8training\src\pages\meta-principal\meta-principal.html"*/'<ion-header>\n\n  <ion-navbar color="objetivo">\n\n    <ion-title>Meta Principal</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n  <ion-item>\n\n    <ion-label>Escreva sua Meta abaixo</ion-label>\n\n  </ion-item>\n\n  \n\n  <ion-textarea padding class="campo-declaracao" placeholder="" [(ngModel)]="meta"></ion-textarea>\n\n\n\n  <button padding ion-button full color="objetivo" (click)=\'editarMeta()\'>Editar</button>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"D:\Projects\g8training\src\pages\meta-principal\meta-principal.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_0__providers_users_users__["b" /* UsersProvider */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["q" /* ToastController */]])
], MetaPrincipalPage);

//# sourceMappingURL=meta-principal.js.map

/***/ })

});
//# sourceMappingURL=20.js.map