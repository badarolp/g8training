webpackJsonp([33],{

/***/ 454:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DiarioDeBordoPageModule", function() { return DiarioDeBordoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__diario_de_bordo__ = __webpack_require__(676);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DiarioDeBordoPageModule = (function () {
    function DiarioDeBordoPageModule() {
    }
    return DiarioDeBordoPageModule;
}());
DiarioDeBordoPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__diario_de_bordo__["a" /* DiarioDeBordoPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__diario_de_bordo__["a" /* DiarioDeBordoPage */]),
        ],
    })
], DiarioDeBordoPageModule);

//# sourceMappingURL=diario-de-bordo.module.js.map

/***/ }),

/***/ 676:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DiarioDeBordoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DiarioDeBordoPage = (function () {
    function DiarioDeBordoPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    DiarioDeBordoPage.prototype.navigateToBack = function () {
        this.navCtrl.pop();
    };
    return DiarioDeBordoPage;
}());
DiarioDeBordoPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-diario-de-bordo',template:/*ion-inline-start:"D:\Projects\g8training\src\pages\diario-de-bordo\diario-de-bordo.html"*/'<ion-header>\n\n  <ion-navbar color="primary">\n\n    <ion-title>Diário de Bordo</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n  <ion-list>\n\n      \n\n    <ion-item>\n\n      <ion-label>Selecionar Data</ion-label>\n\n      <ion-datetime displayFormat="DD/MM/YYYY" [(ngModel)]="myDate">"11/11/2017"</ion-datetime>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label stacked>Por que valeu à pena viver o dia de hoje?</ion-label>\n\n      <ion-input type="text"></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label stacked>Se eu pudesse voltar no tempo e mudar algo que aconteceu hoje, o que eu mudaria?</ion-label>\n\n      <ion-input type="text"></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label stacked>Quais as principais ações em sequência, que irei realizar amanhã que contribuirá e muito para chegar a minha META?</ion-label>\n\n      <ion-input type="text"></ion-input>\n\n    </ion-item>\n\n\n\n  </ion-list>\n\n\n\n  <div padding>\n\n    <button ion-button color="primary" block>Salvar</button>\n\n  </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"D:\Projects\g8training\src\pages\diario-de-bordo\diario-de-bordo.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
], DiarioDeBordoPage);

//# sourceMappingURL=diario-de-bordo.js.map

/***/ })

});
//# sourceMappingURL=33.js.map