webpackJsonp([21],{

/***/ 466:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MensagensPageModule", function() { return MensagensPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__mensagens__ = __webpack_require__(688);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MensagensPageModule = (function () {
    function MensagensPageModule() {
    }
    return MensagensPageModule;
}());
MensagensPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__mensagens__["a" /* MensagensPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__mensagens__["a" /* MensagensPage */]),
        ],
    })
], MensagensPageModule);

//# sourceMappingURL=mensagens.module.js.map

/***/ }),

/***/ 688:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MensagensPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MensagensPage = (function () {
    function MensagensPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.toUser = {
            toUserId: '210000198410281948',
            toUserName: 'Shell'
        };
    }
    MensagensPage.prototype.navigateToBack = function () {
        this.navCtrl.pop();
    };
    return MensagensPage;
}());
MensagensPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-mensagens',template:/*ion-inline-start:"D:\Projects\g8training\src\pages\mensagens\mensagens.html"*/'<ion-header>\n\n    <ion-navbar color="primary">\n\n      <ion-title>Mensagens</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n  \n\n<ion-content>\n\n\n\n  <ion-list>\n\n    \n\n    <ion-item navPush="ChatPage" [navParams]="toUser">\n\n      <ion-avatar item-left>\n\n        <img src="assets/imgs/shell.jpg">\n\n      </ion-avatar>\n\n      <h2>Shell</h2>\n\n      <p>ultima mensagem...</p>\n\n    </ion-item>\n\n\n\n  </ion-list>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"D:\Projects\g8training\src\pages\mensagens\mensagens.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */]])
], MensagensPage);

//# sourceMappingURL=mensagens.js.map

/***/ })

});
//# sourceMappingURL=21.js.map