webpackJsonp([31],{

/***/ 456:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GestaoDeTempoPageModule", function() { return GestaoDeTempoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__gestao_de_tempo__ = __webpack_require__(678);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var GestaoDeTempoPageModule = (function () {
    function GestaoDeTempoPageModule() {
    }
    return GestaoDeTempoPageModule;
}());
GestaoDeTempoPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__gestao_de_tempo__["a" /* GestaoDeTempoPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__gestao_de_tempo__["a" /* GestaoDeTempoPage */]),
        ],
    })
], GestaoDeTempoPageModule);

//# sourceMappingURL=gestao-de-tempo.module.js.map

/***/ }),

/***/ 678:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GestaoDeTempoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GestaoDeTempoPage = (function () {
    function GestaoDeTempoPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    GestaoDeTempoPage.prototype.navigateToPage = function (pageName) {
        this.navCtrl.push(pageName);
    };
    GestaoDeTempoPage.prototype.navigateToBack = function () {
        this.navCtrl.pop();
    };
    return GestaoDeTempoPage;
}());
GestaoDeTempoPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-gestao-de-tempo',template:/*ion-inline-start:"D:\Projects\g8training\src\pages\gestao-de-tempo\gestao-de-tempo.html"*/'<ion-header>\n\n    <ion-navbar color="tempo">\n\n      <ion-title>Gestão de Tempo</ion-title>  \n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <ion-card class="etapa-card">\n\n    <ion-item class="titulo">\n\n      <ion-grid>\n\n        <ion-row>\n\n          <ion-col>\n\n            <div>\n\n              <ion-icon class="goTo" name="arrow-dropright-circle" item-left></ion-icon>    \n\n            </div>\n\n          </ion-col>          \n\n          <ion-col>\n\n            <div class="middle">\n\n              Teste das Atividades\n\n            </div>\n\n          </ion-col>\n\n          <ion-col>\n\n            <div class="right">\n\n              0%\n\n            </div>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-grid>\n\n    </ion-item>\n\n  </ion-card>\n\n\n\n  <ion-card class="etapa-card" tappable (click)="navigateToPage(\'JogoDosNumerosPage\')">\n\n    <ion-item class="titulo">\n\n      <ion-grid>\n\n        <ion-row>\n\n          <ion-col>\n\n            <div>\n\n              <ion-icon class="goTo" name="arrow-dropright-circle" item-left></ion-icon>    \n\n            </div>\n\n          </ion-col>          \n\n          <ion-col>\n\n            <div class="middle">\n\n              Jogo dos Números\n\n            </div>\n\n          </ion-col>\n\n          <ion-col>\n\n            <div class="right">\n\n              0%\n\n            </div>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-grid>\n\n    </ion-item>\n\n  </ion-card>\n\n\n\n</ion-content>'/*ion-inline-end:"D:\Projects\g8training\src\pages\gestao-de-tempo\gestao-de-tempo.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
], GestaoDeTempoPage);

//# sourceMappingURL=gestao-de-tempo.js.map

/***/ })

});
//# sourceMappingURL=31.js.map