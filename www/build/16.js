webpackJsonp([16],{

/***/ 471:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModulosPageModule", function() { return ModulosPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modulos__ = __webpack_require__(693);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ModulosPageModule = (function () {
    function ModulosPageModule() {
    }
    return ModulosPageModule;
}());
ModulosPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__modulos__["a" /* ModulosPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__modulos__["a" /* ModulosPage */])
        ],
    })
], ModulosPageModule);

//# sourceMappingURL=modulos.module.js.map

/***/ }),

/***/ 693:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModulosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ModulosPage = (function () {
    function ModulosPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ModulosPage.prototype.navigateToPage = function (pageName) {
        this.navCtrl.push(pageName);
    };
    ModulosPage.prototype.navigateToBack = function () {
        this.navCtrl.pop();
    };
    return ModulosPage;
}());
ModulosPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-modulos',template:/*ion-inline-start:"D:\Projects\g8training\src\pages\modulos\modulos.html"*/'<ion-header no-border>\n\n  <ion-navbar color="primary">\n\n    <ion-title>Módulos</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n  <ion-card tappable (click)="navigateToPage(\'ObjetivoPage\')">\n\n    <img class="modulo" src="assets/imgs/objetivo.jpg">\n\n    <div class="card-title">Objetivo</div>\n\n  </ion-card>      \n\n\n\n  <ion-card tappable (click)="navigateToPage(\'MeuPerfilPage\')">\n\n    <img class="modulo" src="assets/imgs/perfil.jpg">\n\n    <div class="card-title">Meu Perfil</div>\n\n  </ion-card>      \n\n\n\n  <ion-card tappable (click)="navigateToPage(\'PlanejamentoPage\')">\n\n    <img class="modulo" src="assets/imgs/planejamento.jpg">\n\n    <div class="card-title">Planejamento</div>\n\n  </ion-card>      \n\n\n\n  <ion-card tappable (click)="navigateToPage(\'GestaoDeTempoPage\')">\n\n    <img class="modulo" src="assets/imgs/tempo.jpg">\n\n    <div class="card-title">Gestão de Tempo</div>\n\n  </ion-card>      \n\n  \n\n  <ion-card tappable (click)="navigateToPage(\'GestaoDePontosFracosPage\')">\n\n    <img class="modulo" src="assets/imgs/pontosfracos.jpg">\n\n    <div class="card-title">Gestão de Prontos Fracos</div>\n\n  </ion-card>      \n\n\n\n  <ion-card tappable (click)="navigateToPage(\'ComunicacaoPessoalPage\')">\n\n    <img class="modulo" src="assets/imgs/comunicacao.jpg">\n\n    <div class="card-title">Comunicação Pessoal</div>\n\n  </ion-card>      \n\n\n\n  <ion-card>\n\n    <img class="cadeado" src="assets/imgs/cadeado.png">\n\n    <img class="modulo" src="assets/imgs/foco.jpg">\n\n    <div class="card-title">Foco</div>\n\n  </ion-card>      \n\n      \n\n  <ion-card>\n\n    <img class="cadeado" src="assets/imgs/cadeado.png">\n\n    <img class="modulo" src="assets/imgs/indicadores.jpg">\n\n    <div class="card-title">Indicadores de Desempenho</div>\n\n  </ion-card>      \n\n\n\n</ion-content>'/*ion-inline-end:"D:\Projects\g8training\src\pages\modulos\modulos.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
], ModulosPage);

//# sourceMappingURL=modulos.js.map

/***/ })

});
//# sourceMappingURL=16.js.map