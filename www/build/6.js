webpackJsonp([6],{

/***/ 483:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SlideWalkthroughPageModule", function() { return SlideWalkthroughPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__slide_walkthrough__ = __webpack_require__(705);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SlideWalkthroughPageModule = (function () {
    function SlideWalkthroughPageModule() {
    }
    return SlideWalkthroughPageModule;
}());
SlideWalkthroughPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__slide_walkthrough__["a" /* SlideWalkthroughPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__slide_walkthrough__["a" /* SlideWalkthroughPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__slide_walkthrough__["a" /* SlideWalkthroughPage */]
        ]
    })
], SlideWalkthroughPageModule);

//# sourceMappingURL=slide-walkthrough.module.js.map

/***/ }),

/***/ 705:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SlideWalkthroughPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SlideWalkthroughPage = (function () {
    function SlideWalkthroughPage(navCtrl, viewCtrl) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.slideIndex = 0;
        this.slides = [
            {
                title: 'Aumente Seu Potencial Comunicativo',
                description: 'O teste a seguir ajudará você a descobrir seu sistema de comunicação dominante',
                img: "assets/imgs/voz.png",
            },
            {
                description: 'Escolha 20 palavras na lista a seguir, por qualquer razão',
                img: "assets/imgs/escutar.png",
            },
            {
                description: 'Podem ser marcadas as que mais o impressionam ou as que mais se destacam na sua percepção',
                img: "assets/imgs/visao.png",
            }
        ];
    }
    SlideWalkthroughPage.prototype.onSlideChanged = function () {
        this.slideIndex = this.slider.getActiveIndex();
        console.log('Slide changed! Current index is', this.slideIndex);
    };
    SlideWalkthroughPage.prototype.goToApp = function () {
        this.dismiss();
        console.log('Go to App clicked');
    };
    SlideWalkthroughPage.prototype.skip = function () {
        this.dismiss();
        console.log('Skip clicked');
    };
    SlideWalkthroughPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return SlideWalkthroughPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* ViewChild */])('slider'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Slides */])
], SlideWalkthroughPage.prototype, "slider", void 0);
SlideWalkthroughPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-slide-walkthrough',template:/*ion-inline-start:"D:\Projects\g8training\src\pages\slide-walkthrough\slide-walkthrough.html"*/'<ion-header no-border>\n  <ion-navbar transparent>\n    <ion-buttons right>\n      <button ion-button color="comunicacao"\n              *ngIf="slideIndex < slides.length - 1"\n              class="skip-button"\n              (click)="skip()">PULAR</button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content fullscreen="true" class="no-padding-top">\n  <ion-slides #slider pager="true" autoplay="3000" (ionSlideWillChange)="onSlideChanged()">\n    <ion-slide *ngFor="let slide of slides"\n               class="slide-background"\n               [ngStyle]="{\'background-image\': \'url(\' + slide.imageUrl +\')\'}">\n      <div class="text-wrapper">\n        <div class="slide-text">\n\n          <div class="flb">\n            <div class="Aligner-item Aligner-item--top"></div>\n            <img src={{slide.img}}>\n            <div class="Aligner-item Aligner-item--bottom"></div>\n          </div>\n\n          <h2 class="slide-title" [innerHTML]="slide.title"></h2><br>\n          <p [innerHTML]="slide.description"></p>\n        </div>\n      </div>\n    </ion-slide>\n  </ion-slides>\n  <div class="floating-buttons pop-in" *ngIf="slideIndex >= slides.length - 1">\n    <button ion-button clear large full (click)="goToApp()">Começar!</button>\n  </div>\n</ion-content>'/*ion-inline-end:"D:\Projects\g8training\src\pages\slide-walkthrough\slide-walkthrough.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* ViewController */]])
], SlideWalkthroughPage);

//# sourceMappingURL=slide-walkthrough.js.map

/***/ })

});
//# sourceMappingURL=6.js.map