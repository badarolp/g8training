webpackJsonp([27],{

/***/ 460:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapaTabsPageModule", function() { return MapaTabsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__mapa_tabs__ = __webpack_require__(682);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MapaTabsPageModule = (function () {
    function MapaTabsPageModule() {
    }
    return MapaTabsPageModule;
}());
MapaTabsPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__mapa_tabs__["a" /* MapaTabsPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__mapa_tabs__["a" /* MapaTabsPage */]),
        ],
    })
], MapaTabsPageModule);

//# sourceMappingURL=mapa-tabs.module.js.map

/***/ }),

/***/ 682:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapaTabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MapaTabsPage = (function () {
    function MapaTabsPage() {
        this.tab1Root = 'Mapa1MesPage';
        this.tab2Root = 'Mapa2MesesPage';
        this.tab3Root = 'Mapa3MesesPage';
        this.tab4Root = 'Mapa4MesesPage';
        this.tab5Root = 'Mapa5MesesPage';
    }
    return MapaTabsPage;
}());
MapaTabsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-mapa-tabs',template:/*ion-inline-start:"D:\Projects\g8training\src\pages\mapa-tabs\mapa-tabs.html"*/'<ion-tabs color="planejamento" >\n\n    <ion-tab [root]="tab1Root" tabTitle="1º Mês" tabIcon="ios-git-commit"></ion-tab>\n\n    <ion-tab [root]="tab2Root" tabTitle="2º Mês" tabIcon="ios-git-compare"></ion-tab>\n\n    <ion-tab [root]="tab3Root" tabTitle="3º Mês" tabIcon="ios-git-branch"></ion-tab>\n\n    <ion-tab [root]="tab4Root" tabTitle="4º Mês" tabIcon="ios-qr-scanner"></ion-tab>\n\n    <ion-tab [root]="tab5Root" tabTitle="5º Mês" tabIcon="ios-ionic-outline"></ion-tab>\n\n</ion-tabs>\n\n  \n\n  '/*ion-inline-end:"D:\Projects\g8training\src\pages\mapa-tabs\mapa-tabs.html"*/,
    }),
    __metadata("design:paramtypes", [])
], MapaTabsPage);

//# sourceMappingURL=mapa-tabs.js.map

/***/ })

});
//# sourceMappingURL=27.js.map